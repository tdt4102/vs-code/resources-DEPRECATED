// Algorithm - for each, unique copy, merge, transform, set operations
// Inspired by https://en.cppreference.com/w/cpp/algorithm/for_each

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

struct PrintGreaterThanValue {
	PrintGreaterThanValue(int v) : value{ v } { }
	int value = 0;
	void operator()(int x) { 
		if (x > value) {
			cout << " " << x;
		}
	}
};

void print(const int& n) {
	cout << " " << n;
}

int myFunction(int a, int b) {
	return ((a / 100) + b);
}

int main() {
	{// Part 1, for_each
        vector<int> numberVector {1, 2, 3, 100, 200, 300};
		
        // The for_each template in <algorithm> applies a function on each element in a container, such as a vector
		cout << "Part 1: numberVector using for_each:";
        for_each(numberVector.begin(), numberVector.end(), print);
		cout << endl;
        cout << "Part 1: numberVector using for_each and PrintGreaterThanValue:";
		for_each(numberVector.begin(), numberVector.end(), PrintGreaterThanValue(100));
		cout << endl;
	}
	cout << endl;

	{// Part 2, unique_copy
		vector<int> inputVector {1, 2, 2, 3, 2, 2, 4, 4, 4, -7, 8};
		// Reserve sufficient space in destination
        vector<int> outputVector(inputVector.size()); 
		
        // The unique_copy algorithm finds unique elements in inputVector, and stores them in outputVector
        unique_copy(inputVector.begin(), inputVector.end(), outputVector.begin());
		
        // We use for_each from the previous demonstration to print the results.
        // Notice that the last values in outputVector have been set to 0
        cout << "Part 2: outputVector after using unique_copy:";
        for_each(outputVector.begin(), outputVector.end(), print);
		cout << endl;

        // Now we don't exactly like to zeroes at the end of the vector in the previous example
        // Fortunately, it's possible to remove them. For the remainder of this part we will see how.

        vector<int> vectorWithDuplicates {1, 2, 2, 3, 2, 2, 4, 4, 4, -7, 8};
        // We again make sure to reserve sufficient space
        vector<int> anotherOutputVector(vectorWithDuplicates.size()); 

        // many of the algorithms, such as unique_copy also have a return-value
        // https://en.cppreference.com/w/cpp/algorithm/unique_copy says the return value is an 
        // Output iterator to the element past the last written element 
        vector<int>::iterator it = unique_copy(vectorWithDuplicates.begin(), vectorWithDuplicates.end(), anotherOutputVector.begin());
        
        // If we resize the output vector to the number of elements the iterator has advanced, we get the number of elements that
        // the unique_copy algorithm ended up storing in anotherOutputVector. 
        unsigned int numberOfUniqueElementsInserted = distance(anotherOutputVector.begin(), it);

        // And we can resize the output vector such that we are left with only the unique elements.
        anotherOutputVector.resize(numberOfUniqueElementsInserted);

        // Print the results
        // Notice that the zeroes are now missing :)
        cout << "Part 2: anotherOutputVector after using unique_copy and resized afterwards:";
        for_each(anotherOutputVector.begin(), anotherOutputVector.end(), print);
        cout << endl;
	}
	cout << endl;

	{// Part 3, merge
		vector<int> numberVector {1, 2, 3, 100, 200, 300};
		vector<int> anotherNumberVector {3, 4, 5, 6, 800, 10000}; 
        // Allocate space for the combined size of the two vectors
		vector<int> destination(numberVector.size() + anotherNumberVector.size());

        // The merge algorithm takes two vectors that are already sorted, and creates one large sorted vector containing both.
		merge(numberVector.begin(), numberVector.end(), anotherNumberVector.begin(), anotherNumberVector.end(), destination.begin());
		
        cout << "Part 3: Output of the merge algorithm:";
        for_each(destination.begin(), destination.end(), print);
		cout << endl;	
	}
	cout << endl;

	{// Part 4, transform
		string text = "hello";
		cout << "Part 4: text before: " << text;

        // The transform algorithm applies a function on each element in a container, and stores it in an output container
        // In this case, we can iterate over a string, and use the toupper function to convert each character to uppercase
		transform(text.begin(), text.end(), text.begin(), ::toupper);

		cout << ", and after: " << text;
		cout << endl;

		vector<int> numberVector{1000, 2000, 10000, 20000};
		vector<int> anotherNumberVector{3, 9, 21, 64};

		// You can also supply your own function
		transform(numberVector.begin(), numberVector.end(), anotherNumberVector.begin(), anotherNumberVector.begin(), myFunction);
		
        // Notice that the old values of anotherNumberVector have been overwritten by the transform operation
        cout << "Part 4: transformed vector using myFunction: ";
        for (int e : anotherNumberVector) {
            cout << e << " ";
        }
		cout << endl;
	}
    cout << endl;

	{// Part 5, set_operations
		vector<int> set1 { 10, 20, 30, 40, 50 };
		vector<int> set2 {             40, 50, 60, 70};
		vector<int> resultSet (set1.size() + set2.size());

        // Compute the union of two sets and store the result in another
		set_union(set1.begin(), set1.end(), set2.begin(), set2.end(), resultSet.begin());
		cout << "Part 5: Set union: ";
        for (int e : resultSet) { 
            cout << e << " "; 
        }
        cout << endl;

        // Reset the result set
		resultSet.clear(); 
		resultSet.resize(min(set1.size(), set2.size()));
		
        // Compute the intersection of two sets and store the result in another
        set_intersection(set1.begin(), set1.end(), set2.begin(), set2.end(), resultSet.begin());
        cout << "Part 5: Set intersection: ";
		for (int e : resultSet) {
            cout << e << " ";
        } 
        cout << endl;

		resultSet.clear();
		resultSet.resize(set1.size());

        // Compute the set difference of two sets and store the result in another
		set_difference(set1.begin(), set1.end(), set2.begin(), set2.end(), resultSet.begin());
        cout << "Part 5: Set difference: ";
		for (int e : resultSet) {
            cout << e << " ";
        } 
        cout << endl;

	}

	return 0;
}