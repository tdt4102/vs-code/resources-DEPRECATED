#include "std_lib_facilities.h"

// Example 1: Function declaration and definitions

// This is a function declaration:
bool isGreater(int a, int b);

// Note that this function does not have the {} brackets, 
// and neither any lines of code that would have been inside of those brackets.
// Also note that there is a ; at the end of the line.

// This is a function definition:
bool isGreater(int a, int b) {
    return a > b;
}

// Things to note:
// - The return type is the same
// - The function name is the same
// - The paramters are the same 
// - The {} brackets are back
// - The function does something (check whether a > b)

// But who cares? 
// This sounds super theoretical, but why is it relevant?

// Here's the problem: the compiler looks at a file from the top to the bottom
// This is more or less the same as Python:
// You have to create a function (def isGreater(a, b): for example)
// And can use that function below.
// C++ is no different, except that you can also tell the compiler
// that a particular function will show up later down the line
// That is what a declaration is for:
// It allows you to use the function before it is defined further down the line

// Let's look at an example:

void functionA() {
    cout << "I am function A" << endl;
}

void functionB();

void functionC() {
    cout << "I am function C" << endl;
}

void functionB() {
    cout << "I am function B" << endl;
}

// Here functionA would not be able to use functionB,
// Because at that point functionB has neither been declared nor defined.

// FunctionC is able to use functionB, because it has been declared above it

// FunctionB is able to use both functionA and functionC, because they both have
// been defined above functionB.



// Here's a main function in case you want to play around a bit with these:

int main() {
    functionA();
    functionB();
    functionC();

    return 0;
}


