#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
	while(!window.should_close()) {
		Point mouseCoordinates = window.get_mouse_coordinates();
		string positionString = to_string(mouseCoordinates.x) + ", " + to_string(mouseCoordinates.y);
		window.draw_text(mouseCoordinates, positionString);
		window.next_frame();
	}
}