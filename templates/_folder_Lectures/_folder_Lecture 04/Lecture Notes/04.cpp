// These are the notes that we use for the live coding parts of the regular lectures
// They are made to help us (the lecturers) remember what to show and talk about.
// Also, importantly, they will not compile!

// ---------------------------------------------------------------------------------

// REPTITION

// Show: basic usage of AnimationWindow
#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
	AnimationWindow window;
	window.draw_circle({100, 100}, 50);
	window.wait_for_close();
}

// NEXT TOPIC: ANIMATION

// Show: same drawing as before using the animation method
AnimationWindow window;
while(!window.should_close()) {
	window.draw_circle({100, 100}, 50);
	window.next_frame();
}

// Show: simple movement
AnimationWindow window;
int xCoordinate = 0;
while(!window.should_close()) {
	xCoordinate++;
	window.draw_circle({xCoordinate, 100}, 50);
	window.next_frame();
}

// Show: live x coordinate displayed on screen
AnimationWindow window;
int xCoordinate = 0;
while(!window.should_close()) {
	xCoordinate++;
	window.draw_circle({xCoordinate, 100}, 50);
	window.draw_text({20, 20}, "x coordinate: " + to_string(xCoordinate));
	window.next_frame();
}

// Show: simple movement with reset
AnimationWindow window;
int xCoordinate = 0;
while(!window.should_close()) {
	xCoordinate++;
	if(xCoordinate > 500) {
		xCoordinate = 0;
	}
	window.draw_circle({xCoordinate, 100}, 50);
	window.next_frame();
}

// Show: diagonal movement
AnimationWindow window;
int xCoordinate = 0;
int yCoordinate = 0;
while(!window.should_close()) {
	xCoordinate++;
	yCoordinate++;
	if(xCoordinate > 500) {
		xCoordinate = 0;
	}
	if(yCoordinate > 500) {
		yCoordinate = 0;
	}
	window.draw_circle({xCoordinate, yCoordinate}, 50);
	window.next_frame();
}

// Show: bounce in one corner
AnimationWindow window;
int xCoordinate = 200;
int yCoordinate = 0;
int directionX = 1;
int directionY = 1;
while(!window.should_close()) {
	xCoordinate += directionX;
	yCoordinate += directionY;
	if(xCoordinate > 500) {
		directionX = -1;
	}
	if(yCoordinate > 500) {
		directionY = -1;
	}
	window.draw_circle({xCoordinate, yCoordinate}, 50);
	window.next_frame();
}

// Show: bounce continuously
AnimationWindow window;
int xCoordinate = 200;
int yCoordinate = 0;
int directionX = 1;
int directionY = 1;
while(!window.should_close()) {
	xCoordinate += directionX;
	yCoordinate += directionY;
	if(xCoordinate > 500) {
		directionX = -1;
	}
	if(xCoordinate < 50) {
		directionX = 1;
	}
	if(yCoordinate > 500) {
		directionY = -1;
	}
	if(yCoordinate < 50) {
		directionY = 1;
	}
	window.draw_circle({xCoordinate, yCoordinate}, 50);
	window.next_frame();
}

// Show: osscilation with sin
int frame = 0;
while(!window.should_close()) {
	frame++;
	int xCoordinate = 200 + int(100 * sin(frame / 100.0));
	window.draw_circle({xCoordinate, 100}, 50);
	window.next_frame();
}

// Show: getting a rotation motion with cos
AnimationWindow window;
int frame = 0;
while(!window.should_close()) {
	frame++;
	int xCoordinate = 200 + int(100 * sin(frame / 100.0));
	int yCoordinate = 200 + int(100 * cos(frame / 100.0));
	window.draw_circle({xCoordinate, yCoordinate}, 50);
	window.next_frame();
}

// Show: mouse coordinates
while(!window.should_close()) {
	Point mouseCoordinates = window.get_mouse_coordinates();
	string positionString = to_string(mouseCoordinates.x) + ", " + to_string(mouseCoordinates.y);
	window.draw_text(mouseCoordinates, positionString);
	window.next_frame();
}

// Show: draw circle at mouse coordinates
AnimationWindow window;
while(!window.should_close()) {
	Point mouseCoordinates = window.get_mouse_coordinates();
	window.draw_circle({int(mouseCoordinates.x), int(mouseCoordinates.y)}, 50);
	window.next_frame();
}

// Show: keep a set of coordinates where the circle should be drawn, 
// and move it towards the mouse
AnimationWindow window;
double xCoordinate = 0;
double yCoordinate = 0;
while(!window.should_close()) {
	Point mouseCoordinates = window.get_mouse_coordinates();
	double deltaX = mouseCoordinates.x - xCoordinate;
	double deltaY = mouseCoordinates.y - yCoordinate;
	xCoordinate += deltaX;
	yCoordinate += deltaY;
	window.draw_circle({int(xCoordinate), int(yCoordinate)}, 50);
	window.next_frame();
}

// Show: slow down movement
AnimationWindow window;
double xCoordinate = 0;
double yCoordinate = 0;
while(!window.should_close()) {
	Point mouseCoordinates = window.get_mouse_coordinates();
	double deltaX = mouseCoordinates.x - xCoordinate;
	double deltaY = mouseCoordinates.y - yCoordinate;
	xCoordinate += deltaX * 0.01;
	yCoordinate += deltaY * 0.01;
	window.draw_circle({int(xCoordinate), int(yCoordinate)}, 50);
	window.next_frame();
}



// NEXT TOPIC: VECTORS AND ARRAYS

// Show: Basic usage of vector
// ! SHOW VECTOR ELEMENTS IN DEBUGGER
vector<int> vec;
vec.push_back(1);
vec.push_back(2);
vec.push_back(3);
cout << "Index 0: " << vec.at(0) << endl;
cout << "Index 1: " << vec.at(1) << endl;
cout << "Index 2: " << vec.at(2) << endl;

// Show: using at() to assign values
vector<int> vec;
vec.push_back(1);
vec.push_back(2);
vec.push_back(3);
vec.at(0) = vec.at(1) + vec.at(2) * 10;
vec.at(1)++;
vec.at(2) += 10;
cout << "Index 0: " << vec.at(0) << endl;
cout << "Index 1: " << vec.at(1) << endl;
cout << "Index 2: " << vec.at(2) << endl;

// Show: size keeps on increasing
// ! maybe use task manager to show memory usage increasing
vector<double> memoryOccupier;
while(true) {
	memoryOccupier.push_back(8);
}

// Show: can also initialise to predefined values
vector<int> vec {10, 20, 30, 40, 50};

// Show: can create a vector with a specific size
// ! Show all values are 0 in debugger
vector<double> anotherVector(100);

// Show: iterating over a vector
vector<int> vec {10, 20, 30, 40, 50};
for(int i = 0; i < vec.size(); i++) {
	cout << "Index " << i << " contains the value " << vec.at(i) << endl;
}

// Show: using the size() function to show size
vector<int> vec {10, 20, 30, 40, 50};
cout << "The vector now contains " << vec.size() << " element(s)" << endl;

// Show: out of bounds exceptions using .at()
vector<int> vec {10, 20, 30, 40, 50};
cout << vec.at(-1) << endl;
cout << vec.at(5) << endl;

// Show: can't use cout
vector<int> vec {10, 20, 30, 40, 50};
cout << vec << endl;

// Show: removing elements with resize()
// ! Use debugger to show elements that remain
vector<double> vec {1, 2, 3, 4, 5};
cout << "Size before resize: " << vec.size() << endl;
vec.resize(2);
cout << "Size after resize: " << vec.size() << endl;

// Show: removing elements is kind of weird
vector<double> vec {1, 2, 3, 4, 5};
vec.erase(vec.begin() + 2);

// Show: basic array
// ! Use debugger to show uninitialised values
array<double, 10> arr {1.0, 2.0, 3.0, 5.0, 1.0, 5.0};

// Show: at() works the same as with vector
array<double, 10> arr {1.0, 2.0, 3.0, 5.0, 1.0, 5.0};
arr.at(0) = arr.at(5) + arr.at(6) / 2;

// Show: no push_back()
array<unsigned int, 30> anotherArray;
anotherArray.push_back(10); // Does not compile

// Show: array has fill method that vector does not
// ! show result in debugger
array<unsigned int, 30> anotherArray;
anotherArray.fill(50);

// Show: indexing using []
vector<int> vec {1, 2, 3, 4, 5};
cout << vec[1] << endl;
cout << vec.at(1) << endl;

// Show: out of bounds error with at()
vector<int> vec {1, 2, 3, 4, 5};
cout << vec[5] << endl;
cout << vec.at(5) << endl;

// Show: alternate syntax
int vec[5] {1, 2, 3, 4, 5};
cout << vec[5] << endl;
cout << vec.at(5) << endl; // Does not compile!

// Show: why you should use at() -> program prints "5"
// ! compiler may behave differently. Try index -1 if it doesn't work
void doDarkMagic() {
	int value = 10;
	array<int, 5> list;
	list[5] = 5; // index is -1 on linux
	cout << value << endl;
}

int main() {
	doDarkMagic();
	return 0;
}

// Show: you can even hang the debugger if you're not careful
// ! Tested on Linux and Windows
void doDarkMagic() {
	array<int, 5> list;
	list[7] = 0;
	list[8] = 0;
}

int main() {
	doDarkMagic();
	return 0;
}

// The result of the above: https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=%22buffer+overflow%22

// NEXT TOPIC: RANDOM NUMBERS
// Note: for comedic effect, only run each example once.
// Do not show it is the same every time until the lottery joke :)

// Show: a basic random number generator
default_random_engine engine;
cout << engine() << endl;
for(int i = 0; i < 50; i++) {
	cout << engine() << endl;
}

// Show: uniform real distribution
default_random_engine engine;
uniform_real_distribution dist(0, 100);
for(int i = 0; i < 50; i++) {
	cout << dist(engine) << endl;
}

// Show: uniform integer distribution
default_random_engine engine;
uniform_int_distribution dist(0, 100);
for(int i = 0; i < 50; i++) {
	cout << dist(engine) << endl;
}

// Show: we need to seed a random number generator
// Mac: 48270
// Windows + Linux: 8403
default_random_engine engine;
uniform_int_distribution dist(0, 1000000000);
cout << "Pick your lucky number: ";
int luckyNumber;
cin >> luckyNumber;
cout << "The winning number is: " << (dist(engine)) << endl;
cout << "Your lucky number was: " << luckyNumber << endl;

// Show: seeding a random number generator with a fixed seed
// Mac: 24135499
// Windows + Linux: 4201749
default_random_engine engine(500);
uniform_int_distribution dist(0, 1000000000);
cout << "Pick your lucky number: ";
int luckyNumber;
cin >> luckyNumber;
cout << "The winning number is: " << (dist(engine)) << endl;
cout << "Your lucky number was: " << luckyNumber << endl;

// Show: random_device produces a random number using the OS random number generator
random_device device;
for(int i = 0; i < 50; i++) {
	cout << "Random device: " << device() << endl;
}

// Show: seeding a random number generator with a random seed
random_device device;
default_random_engine engine(device());
uniform_int_distribution dist(0, 1000000000);
cout << "Pick your lucky number: ";
int luckyNumber;
cin >> luckyNumber;
cout << "The winning number is: " << (dist(engine)) << endl;
cout << "Your lucky number was: " << luckyNumber << endl;

// Show: The random function is not all that complicated
unsigned int random(unsigned int previousNumber) {
	unsigned long multiplier = 16807;
	unsigned long divider = 2147483647;
	return (multiplier * previousNumber) % divider;
}

int main() {
	default_random_engine engine;

	cout << "Using the standard library: " << endl;
	cout << engine() << endl;
	cout << engine() << endl;
	cout << engine() << endl;
	cout << engine() << endl;
	cout << endl;

	cout << "Using our own version: " << endl;
	unsigned int randomValue0 = generateRandomNumber(1);
	unsigned int randomValue1 = generateRandomNumber(randomValue0);
	unsigned int randomValue2 = generateRandomNumber(randomValue1);
	unsigned int randomValue3 = generateRandomNumber(randomValue2);
	cout << randomValue0 << endl;
	cout << randomValue1 << endl;
	cout << randomValue2 << endl;
	cout << randomValue3 << endl;
}

// we are vectorious