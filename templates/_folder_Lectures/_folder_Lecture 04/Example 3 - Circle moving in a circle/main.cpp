#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
	AnimationWindow window;
	int frame = 0;
	while(!window.should_close()) {
		frame++;
		int xCoordinate = 200 + int(100 * sin(frame / 100.0));
		int yCoordinate = 200 + int(100 * cos(frame / 100.0));
		window.draw_circle({xCoordinate, yCoordinate}, 50);
		window.next_frame();
	}
}