#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
	AnimationWindow window;
	int xCoordinate = 200;
	int yCoordinate = 0;
	int directionX = 1;
	int directionY = 1;
	while(!window.should_close()) {
		xCoordinate += directionX;
		yCoordinate += directionY;
		if(xCoordinate > 500) {
			directionX = -1;
		}
		if(xCoordinate < 50) {
			directionX = 1;
		}
		if(yCoordinate > 500) {
			directionY = -1;
		}
		if(yCoordinate < 50) {
			directionY = 1;
		}
		window.draw_circle({xCoordinate, yCoordinate}, 50);
		window.next_frame();
	}
	return 0;
}