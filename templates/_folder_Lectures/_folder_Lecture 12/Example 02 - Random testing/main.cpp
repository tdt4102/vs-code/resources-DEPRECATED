#include <random>
#include <cassert>
#include <iostream>

void sort(std::vector<int>& values) {
	// This is an algorithm known as "bubble sort"
	// It's a terrible algorithm that's horrendously slow, but 
	// often used as an example because of its simplicity.
	bool isSorted = false;
	while(!isSorted) {
		isSorted = true;
		for(int i = 1; i < values.size(); i++) {
			// If the previous element is greater than the 
			// one following it, we swap the two values

            // For the sake of having a bug in this function, let's
            // have the function behave incorrectly when using small numbers
            bool causeProblems = values.at(i) < 100;

			if(values.at(i - 1) > values.at(i) && !causeProblems) {
				int temp = values.at(i - 1);
				values.at(i - 1) = values.at(i);
				values.at(i) = temp;

				// Since we encountered two values that were not sorted,
				// We will set isSorted to false such that we do another iteration
				// after this one
				isSorted = false;
			}
		}
	}
}

void testOurSortFunction() {
	std::vector<int> testValues(100);
	
	// We start by generating a list with random values
	// Random testing does not necessarily mean a list of random values,
	// But it usually ends up being a list of sorts in practice.
	// However, it is worth noting that for instance object fields 
	// could be randomised as well
	std::random_device device;
	std::default_random_engine engine(device());
	std::uniform_int_distribution<int> distribution(0, 10000000);

	for(int i = 0; i < testValues.size(); i++) {
		testValues.at(i) = distribution(engine);
	}

	// Now that we have some random data to work with, we can 
	// run the function we want to test
	sort(testValues);

	// At this point the list of values should be sorted.
	// We can verify this by ensuring each successive element is bigger
	// than the one that came before it.
	// We will use assert() to notify us if we encounter a case
	// where the list was not sorted correctly. 
	for(int i = 1; i < testValues.size(); i++) {
		assert(testValues.at(i - 1) <= testValues.at(i));
	}
}

int main() {
	// When doing random tests, we generate random data and see if our functions
	// behave as expected. We therefore need to run our test procedure many times,
	// because if some bugs only occur once 
	for(int i = 0; i < 1000; i++) {
		std::cout << "Running test iteration " << i << std::endl;
		testOurSortFunction();
	}

	return 0;
}