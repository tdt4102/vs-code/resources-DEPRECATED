#include <cassert>
#include <string>

bool stringIsEqual(std::string a, std::string b) {
	return a == b;
}

int main() {
	// assert() is a function used often in testing.
	// If the parameter passed into it, the function crashes the program
	// It is used to ensure that the program is not doing anything it shouldn't be
	// Here we use a stringIsEqual() function, which will of course return false
	// because the string "James Bond" is not the same as "Bond".
	// The program will therefore crash as soon as you run it.
	// Give it a try and see what that looks like :)
	assert(stringIsEqual("James Bond", "Bond"));
}