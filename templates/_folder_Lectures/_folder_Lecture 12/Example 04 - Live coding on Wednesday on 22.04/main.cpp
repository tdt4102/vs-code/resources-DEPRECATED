#include <iostream>
#include <exception>

using namespace std;

class ConnectionLostException : public std::exception {
public:
    string _connectionName;
    ConnectionLostException(string connectionName) : _connectionName(connectionName) {
        
    }
    const char* what() const _NOEXCEPT {
        return _connectionName.c_str();
    }
};

class NetworkHandle {
public:
    string _name;
    NetworkHandle(string name) : _name(name) {}
    ~NetworkHandle() {
        cout << "Destructor: " << _name << endl;
    }
};

void functionThatThrows() {
    NetworkHandle b("b");
    throw ConnectionLostException("www.google.com");
    cout << "Kjores aldri" << endl;
}

void functionA() {
    NetworkHandle a("a");
    functionThatThrows();
    cout << "Kjores aldri" << endl;
}

int main() {
    try {
        functionA();
    } catch (std::exception &e) {
        cout << e.what();
    /*} catch (ConnectionLostException &e) {
        cout << e.what();*/
    }
    cout << "Kjoeres dette?" << endl;
}
