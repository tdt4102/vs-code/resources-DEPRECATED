#include <iostream>
#include <exception>
#include <vector>



int main() {
	// Exceptions allow you to handle any unexpected errors that might occur in your program

	// We use exceptions by first creating a try/catch statement. 
	// Within the try{} section, the program will run as normal, and continue
	// past the end of the last catch statement you see underneath it.
	// However, if an exception occurs, it will 
	try {
		std::vector<int> emptyList;
		emptyList.at(3) = 5;
		// Because the line above will cause an exception, 
		// the program will continue at the catch statement that corresponds with the
		// error type you put in. Therefore, the line below will not be executed.
		std::cout << "This line will not be printed" << std::endl;

	  // When an error occurs, C++ will check each catch statement that handles the
	  // exception type that was thrown within the try{} region.
      // In this case, the at() function will throw a std::out_of_range exception.
	  // This catch statement takes in an exception of type std::out_of_range, which
	  // means this catch section will be executed when the out_of_range error occurs.
	} catch(std::out_of_range& exception) {
		std::cout << "A std::out_of_range exception has occurred!" << std::endl;
		// The what() function is used by exceptions to provide more information about
		// what exactly went wrong when the error was encountered.
		std::cout << "Message: " << exception.what() << std::endl;
	// It is possible to create different catch statements to handle different types of
	// errors. If the type of exception that was thrown within the try{} region was NOT of
	// type std::out_of_range, the exception type will be matched against the next region instead
	} catch(std::runtime_error& error) {
		std::cout << "This exception handler will not be called" << std::endl;
	}

	// If you want to raise your own exceptions, you can use the throw statement
	// Let's start by creating another try/catch statement:
	try {
		// We will now run some code that can cause problems.
		// We will ask the user to enter a number
		int value = 0;
		std::cout << "Enter a number: ";
		std::cin >> value;
		// Now we check for whether the entered number was valid (that is, positive)
		if(value < 0) {
			// If not, we use the throw keyword, followed by an exception data type
			// The std::runtime_error is quite appropriate to use here
			// The constructor of std::runtime_error takes a single parameter, which
			// is an error message of some kind.
			throw std::runtime_error("The entered value was " + std::to_string(value) + ", which is not allowed to be negative");
		}
		// If the value entered by the user was valid, the value is printed out as normal
		std::cout << "The entered value was " << value << std::endl;

	// Here is where we will handle the std::runtime_error if something happens
	} catch(std::runtime_error& error) {
		std::cout << "Caught an error!" << std::endl;
		// The what() function will print out the message we put into the constructor of std::runtime_error
		std::cout << "The message contained within the exception was: " << error.what() << std::endl;
	}


	// One more thing: catch statements work with inheritance.

	// That is, all C++ built-in C++ exceptions inherit from the base class std::exception,
	// and when an error occurs, the catch statement that will be run is the first one
	// in the list of catch statements where the exception type is either 
	// _exactly_ the same as the error that occurred, OR one of its parent classes.

	// For example, in the example below an exception of std::logic_error is thrown. 
	// Since all built-in C++ exceptions inherit from std::exception,
	// std::logic_error is also a std::exception, and is matched against the first catch statement.

	// Since catch statements are matched in the order they are specified,
	// the more specific std::logic_error is not used in this case.

	// Therefore, when handling exceptions which use inheritance, create catch statements for
	// the specific child exceptions first (e.g. std::logic_error or std::runtime_error) first, 
	// and afterwards the more general ones (std::exception)

	// In this case that would concretely mean you should swap the std::exception and std::logic_error
	// catch clauses, as std::logic_error is more specific (a child class of) std::exception.
	try {
		throw std::logic_error("Uh oh!");
	} catch(std::exception& e) {
		std::cout << "This is the handler for std::exception" << std::endl;
		std::cout << "Message: " << e.what() << std::endl;
	} catch(std::logic_error& e) {
		std::cout << "This is the handler for std::logic_error" << std::endl;
		std::cout << "Message: " << e.what() << std::endl;
	}
	return 0;
}