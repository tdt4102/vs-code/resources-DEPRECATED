#include <iostream>
#include <vector>
#include <fstream>

int main() {
    std::ofstream outputFile{"out.txt"};

	std::vector<double> vec;
	for(int i = 0; i < 30000; i++) {
	    // We create a vector that expands over time
        vec.push_back(8);
        
        // The size() method returns the number of elements in the vector
        // We have seen that one before
        // The capacity() method returns the length of the internal array
        // of the vector. Notice how it doubles every time the vector
        // fills it up
	    std::cout << vec.size() << ", " 
	              << vec.capacity() << std::endl;

        // We also write the same output to a file if the 
        // terminal history does not go far back enough to show it all
        outputFile << vec.size() << ", " 
	               << vec.capacity() << std::endl;
	}
}