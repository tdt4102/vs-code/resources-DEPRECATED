#include "Stopwatch.h"

int main() {
    Stopwatch stopwatch;
    stopwatch.start();

    const long repetitions = 100000000;
    for(long i = 0; i < repetitions; i++) {
        // Operation you want to time goes here
    }

    double timeTakenSeconds = stopwatch.stop() / double(repetitions);
    return 0;
}