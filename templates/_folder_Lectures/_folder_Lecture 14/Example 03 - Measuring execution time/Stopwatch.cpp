#include "Stopwatch.h"

void Stopwatch::start() {
    startTime = std::chrono::steady_clock::now();
}

double Stopwatch::stop() {
    std::chrono::time_point endTime = std::chrono::steady_clock::now();
    long durationInMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
    double durationInSeconds = double(durationInMilliseconds)/1000.0;
    std::cout << "Time taken: " << durationInSeconds << " seconds" << std::endl;
    return durationInSeconds;
}