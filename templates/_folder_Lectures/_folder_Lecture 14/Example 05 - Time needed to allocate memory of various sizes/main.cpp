#include "Stopwatch.h"
#include <vector>

int main() {
    unsigned long size = 1;
    for(int k = 0; k < 19; k++) {
        const unsigned long length = 10000000;
        
        std::vector<int*> list;
        list.reserve(length);

        Stopwatch stopwatch;
        stopwatch.start();
        for(unsigned long i = 0; i < length; i++) {
            list.push_back(new int[size]);
        }
        for(unsigned long i = 0; i < length; i++) {
            delete[] list.at(i);
        }
        double timeTaken = stopwatch.stop();
        double timePerAllocation = timeTaken / double(length);
        std::cout << (size * sizeof(int)) << " bytes, " << (timePerAllocation * 1000000000.0) << " nanoseconds per allocation+deallocation" << std::endl;
        size <<= 1;
    }
    
    return 0;
}