#include "Stopwatch.h"
#include <array>
#include <vector>

int main() {
    // This creates a matrix of numbers with a size of
    // matrixSize x matrixSize elements
    const int matrixSize = 25000;
    std::vector<std::array<int, matrixSize>> matrix(matrixSize);

    Stopwatch stopwatch;
    // We will start by iterating over each element in a row by row manner
    std::cout << "Row by Row" << std::endl;
    stopwatch.start();
    for(int row = 0; row < matrixSize; row++) {
        for(int col = 0; col < matrixSize; col++) {
            matrix.at(row).at(col) += 5;
        }
    }
    stopwatch.stop();

    // And now we do the exact same thing in a column by column manner
    std::cout << std::endl << "Column by Column" << std::endl;
    stopwatch.start();
    for(int col = 0; col < matrixSize; col++) {
        for(int row = 0; row < matrixSize; row++) {
            matrix.at(row).at(col) += 5;
        }
    }
    stopwatch.stop();

    return 0;
}