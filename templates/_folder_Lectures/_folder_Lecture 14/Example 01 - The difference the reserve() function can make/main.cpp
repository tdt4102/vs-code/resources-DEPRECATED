#include "Stopwatch.h"

int main() {
    Stopwatch stopwatch;
    stopwatch.start();

    std::vector<int> vec;
    const unsigned long length = 100000000;

    // Try commenting out this line and see how much the amount of time
    // needed by the program changes
    vec.reserve(length);

    for(unsigned long i = 0; i < length; i++) {
        vec.push_back(5);
    }

    stopwatch.stop();
    return 0;
}