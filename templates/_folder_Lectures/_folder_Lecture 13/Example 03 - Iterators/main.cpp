#include <vector>
#include <iostream>

int main() {
	// iterators are a way by which you can iterate over
	// the contents of ANY container in the STL standard library.

	// If you look at the documentation of a container,
	// for example: https://en.cppreference.com/w/cpp/container/deque

	// You can see they all have a begin() and end() method.
	// These can be used to iterate through the contents of the container
	// using a for or while loop:
	std::vector<double> vec(10);
	for(int i = 0; i < 10; i++) {
		vec.at(i) = i;
	}

	for(std::vector<double>::iterator it = vec.begin(); it != vec.end(); it++) {
		std::cout << *it << std::endl;
	}

	// Note the data type of the iterator variable:
	// it is the data type of the container, with ::iterator attached to it
	// This is consistent for all containers.

	// We create an iterator variable it, and set it to the start element
	// Next, we iterate until the iterator is at the end (it != vec.end()),
	// and move on to the next element using the ++ operator after each iteration (it++)

	// Using the value of an iterator is like using a pointer
	// We can "dereference" it using the * operator (*it)


	// The begin() and end() methods are for using a
	// forward iterator. If we want to go into the opposite direction, we need a 
    // reverse iterator instead. Notice that the iterator data type now has ::reverse_iterator 
    // at the end, and we use the rbegin() and rend() methods.
    for(std::vector<double>::reverse_iterator it = vec.rbegin(); it != vec.rend(); it++) {
        std::cout << *it << std::endl;
    }

    // Writing out the iterator data type every time is pretty tedious.
    // The much easier way is using the auto keyword, which lets the compiler figure it out:
    for(auto it = vec.begin(); it != vec.end(); it++) {
        std::cout << *it << std::endl;
    }

    // Note that the value returned by the iterator of different containers can vary
    // a std::unordered_map will have iterator values of std::pair, for example.
}