#include <vector>
#include <iostream>

// If you want to do anything with parameters of your program that are passed through
// the terminal, you have to use the expanded form of the main() function, which
// looks like this:
int main(int argc, char** argv) {

	// argc is the number of arguments that were provided,
	// where the first argument is always the path to the program itself.
	// That means argc is always 1 or greater

	// argv is a C-style array (a pointer) of C-style strings (char*)
	// That is why its datatype is char**.
	// You probably want to turn these into regular strings before
	// proceeding:

	std::vector<std::string> arguments;
	for(int i = 0; i < argc; i++) {
		arguments.push_back(argv[i]);
	}

	// Now we can use the command line options that were provided to run our program
	// You decide yourself how to interpret each of them
	std::cout << "The program was run with the following arguments:" << std::endl;
	for(unsigned int i = 0; i < arguments.size(); i++) {
		std::cout << "Argument " << i << ": " << arguments.at(i) << std::endl;
	}

	// When you open a file with the program you have written using the "open with"
	// function of your operating system (like in File Explorer on Windows or Finder on Mac),
	// The file path that should be opened is given as the second parameter (index 1).

	// When opening many files with the program, the file paths will be given as 
	// additional command line parameters.

	// If you want to support specifying specific values, you really want to use
	// one of a many dozens of libraries out there for interpreting command line options.
	// This is one of those wheels you really don't want to reinvent!
	
	return 0;
}