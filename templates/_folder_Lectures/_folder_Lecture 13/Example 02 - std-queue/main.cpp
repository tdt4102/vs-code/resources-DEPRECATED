#include <iostream>
#include <queue>
#include <string>

int main() {
	// std::queue really is what it sounds like.
	// It is a template, so you need to specify what data type its
	// contents should have
	std::queue<std::string> messages;

	// You can insert new entries into the queue
	messages.push("I am first in line");
	messages.push("I am second");
	messages.push("I am third");

	// The empty() method determines whether there is anything
	// in the queue left to process
	while(!messages.empty()) {
		// The front() method returns the element that is next
		// in the queue. However, it does not remove it
	    std::cout << messages.front() << std::endl;
	    // You therefore need to remove it explicitly using pop()
	    messages.pop();
	}
}