#include <iostream>
#include <string>

// Templates are generic versions of objects or functions,
// where the values of template parameters are substituted with those 
// provided when you try to create an instance of an object or call a function

// Here is a simple template for the Point class we have seen before:
template<typename Type>
struct Point {
    Type x = 0;
    Type y = 0;
};

// After the template keyword follows the <> braces we have seen many times
// before when using them with other templates such as std::vector.
// Here we are specifying the parameters of the template.
// Our Point struct only has one of them. The keyword 'typename' indicates that
// this parameter expects a data type.
// The name Type is one that we can choose ourselves.

// We'll next look at how we can use our template
void usePoint() {
    // When creating an instance of Point, we now need to specify
    // a data type as our template parameter. We'll pick double here:
    Point<double> doublePoint;
    // There is no limit to which data type you can insert here.
    // If you feel like it, you can for instance replace it with std::string.
    // (although since we try assigning 0 to x and y in the declaration of Point,
    // the compiler will complain in this case)

    // If you hold your mouse over the x variable in VS Code, 
    // you can see it has the data type of double.
    doublePoint.x = 5.0; 
}

// The data type of a template can be used anywhere in the class, including its methods.
// All the template does is replace the type parameter with the one you specify later
template<typename Type>
struct BetterPoint {
    Type x = 0;
    Type y = 0;
    
    // We can overload operators, for example
    void operator+=(BetterPoint<Type> other) {
        x += other.x;
        y += other.y;
    }

    // Or return values of the given type
    Type getX() {
        return x;
    }
};

// Templates can also be used on functions.
// Here's how we can define a print() function similar to the one used in Python:
template<typename T>
void print(T value) {
    std::cout << value << std::endl;
}

void usePrint() {
    std::string message = "Hey";
    // Try hovering with your mouse over the print() calls below.
    // See how the parameter has changed to the data type of the value that was passed in?
    print(message);
    print(6);
    print(3.0f);

    // We kind of cheated a bit in the example above.. 
    // The compiler will use the template of print() that corresponds to the
    // data type of the parameter passed in.
    // You can force the exact data type the print template should use
    // in a similar manner to how you would do that with an object instance:
    print<int>(4.66);

    // Note that parameter deduction is most commonly used for functions
    // It does not work as often for objects for various reasons.
}

// Templates can have multiple parameters:
template<typename Type1, typename Type2>
void print(Type1 value1, Type2 value2) {
    std::cout << value1 << ", " << value2 << std::endl;
}

void alsoUsePrint() {
    // We can have the compiler determine the right parameters automatically:
    print(4, 6.6);

    // Or specify them explicitly:
    print<float, float>(1, 4);
}

// Finally, template arguments can also be values
// This is usually used for numeric types
// We have already seen this used before with std::array
template<typename DataType, unsigned int length>
class Queue {
    std::array<DataType, length> contents;
public:
    unsigned int getLength() {
        // We can use it like any other constant variable
        return length;
    }
};

// One final thing:
// While we have declared all templates in the main file here
// when you use separate files, you MUST!! declare and define all methods of objects
// and functions you declare as a template in the header file
// That means you usually don't even create a .cpp file for template objects or functions

int main() {
    usePrint();
    alsoUsePrint();
    return 0;
}