#include <iostream>
#include <vector>

template<typename T>
std::ostream& operator<<(std::ostream& stream, std::vector<T>& vec) {
    stream << "(";
    for(int i = 0; i < vec.size(); i++) {
        stream << vec.at(i);
        bool isLastElement = i == (vec.size() - 1);
        if(!isLastElement) {
            stream << ", ";
        }
    }
    stream << ")";
    return stream;
}