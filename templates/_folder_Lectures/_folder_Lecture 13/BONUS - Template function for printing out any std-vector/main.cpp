#include "printVector.h"

int main() {
    std::vector<int> vec(10);
    for(int i = 0; i < vec.size(); i++) {
    	vec.at(i) = i;
    }

    // Have a look at the function in printVector.h
    // It is a template version of the overloaded << operator for vectors
    // By making it a template, we can use it to print out a vector containing
    // any data type we might desire.
    // Neat right? 
    // We don't need to provide the <> braces because the compiler can figure out
    // which variant of the template you are trying to use automatically.
    
    // I made it a header on purpose so you can copy it and use it in future projects :)
    std::cout << vec << std::endl;
    return 0;
}