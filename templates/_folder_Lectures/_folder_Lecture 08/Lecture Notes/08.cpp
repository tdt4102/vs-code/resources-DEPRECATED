// These are the notes that we use for the live coding parts of the regular lectures
// They are made to help us (the lecturers) remember what to show and talk about.
// Also, importantly, they will not compile!

// ---------------------------------------------------------------------------------

// REPETITION: all in one!
// Show: big example - painful tooth game
// Start by writing main.cpp, then HurtingToothGame.h, and finally everything else

// File: AttemptResult.h
#pragma once

enum class AttemptResult {
	nothingHappened, theBadTooth, triedBefore, gameAlreadyOver, indexOutOfRange
};

// File: HurtingToothGame.h
#pragma once
#include <vector>
#include "AttemptResult.h"

class HurtingToothGame {
	std::vector<bool> triedTeeth;
	int hurtingToothIndex = 0;
	bool gameIsOver = false;

public:
	HurtingToothGame(int count);
	AttemptResult tryTooth(int index);
	bool isGameOver();

	const int toothCount = 0;
};

// File: HurtingToothGame.cpp
#include "HurtingToothGame.h"
#include <random>

HurtingToothGame::HurtingToothGame(int count)
	: triedTeeth(count, false),
  toothCount{count} {
	std::random_device device;
	std::default_random_engine engine(device());
	std::uniform_int_distribution distribution(0, count - 1);
	hurtingToothIndex = distribution(engine);
}

AttemptResult HurtingToothGame::tryTooth(int index) {
	if(gameIsOver) {
		return AttemptResult::gameAlreadyOver;
	} else if(index < 0) {
		return AttemptResult::indexOutOfRange;
	} else if(index >= toothCount) {
		return AttemptResult::indexOutOfRange;
	} else if(triedTeeth.at(index) == true) {
		return AttemptResult::triedBefore;
	}

	triedTeeth.at(index) = true;

	if(index == hurtingToothIndex) {
		gameIsOver = true;
		return AttemptResult::theBadTooth;
	} else {
		return AttemptResult::nothingHappened;
	}
}

bool HurtingToothGame::isGameOver() {
	return gameIsOver;
}


// File: main.cpp
#include "HurtingToothGame.h"
#include <iostream>

int main()
{
	HurtingToothGame game(10);
	while(!game.isGameOver()) {
		std::cout << "Enter the next tooth to guess: ";
		int toothIndex;
		std::cin >> toothIndex;
		AttemptResult result = game.tryTooth(toothIndex);
		switch(result) {
		case AttemptResult::gameAlreadyOver:
			std::cout << "The game is already over!" << std::endl;
			break;
		case AttemptResult::nothingHappened:
			std::cout << "Nothing happened.." << std::endl;
			break;
		case AttemptResult::theBadTooth:
			std::cout << "Oh no! That was the bad tooth. You got bitten!" << std::endl;
			break;
		case AttemptResult::triedBefore:
			std::cout << "You have already tried to guess that tooth before!" << std::endl;
			break;
		case AttemptResult::indexOutOfRange:
			std::cout << "That is not a valid tooth index. It must be between 0 and " << (game.toothCount - 1) << std::endl;
			break;
		}
	}
	std::cout << "Game over!" << std::endl;
	return 0;
}

// NEXT TOPIC: std::filesystem

// Paths

// Show: std::filesystem::path

#include <iostream>
#include <filesystem>

int main()
{
    std::filesystem::path logFilePath {"builddir/meson-logs/meson-log.txt"};
    std::cout << logFilePath.string() << std::endl;
    return 0;
}

// Show: std::filesystem::exists()

    std::filesystem::path logFilePath {"builddir/meson-logs/meson-log.txt"};
    bool logFileExists = std::filesystem::exists(logFilePath);
    std::cout << logFileExists << std::endl;

// Show: std::filesystem::current_path()

    std::filesystem::path workingDirectory = std::filesystem::current_path();
    std::cout << workingDirectory.string() << std::endl;

// Show: std::filesystem::absolute()

    std::filesystem::path logFilePath {"builddir/meson-logs/meson-log.txt"};
    std::filesystem::path absolutePath = std::filesystem::absolute(logFilePath);
    std::cout << logFilePath.string() << std::endl;

// Show: std::filesystem::equivalent()

    std::filesystem::path logFilePath {"builddir/meson-logs/meson-log.txt"};
    std::filesystem::path absolutePath = std::filesystem::absolute(logFilePath);
    bool pathsEquivalent = std::filesystem::equivalent(logFilePath, absolutePath);
    std::cout << pathsEquivalent << std::endl;

// Show: std::filesystem::relative()

    std::filesystem::path logFilePath {"builddir/meson-logs/meson-log.txt"};
    std::filesystem::path subprojectsDirectoryPath {"subprojects"};
    std::filesystem::path relativePath = std::filesystem::relative(logFilePath, subprojectsDirectoryPath);
    std::cout << relativePath.string() << std::endl;


// Operations

// std::filesystem::copy()

#include <iostream>
#include <filesystem>

int main()
{
    std::filesystem::path logFilePath {"builddir"};
    std::filesystem::path targetDirectory {"other"};
    std::filesystem::copy(logFilePath, targetDirectory, std::filesystem::copy_options::recursive);
    return 0;
}

// std::filesystem::create_directories()

    std::filesystem::path pathThatDoesntExist {"some/path/of/directories"};
    std::filesystem::create_directories(pathThatDoesntExist);

// std::filesystem::remove_all()

    std::filesystem::path pathToRemove {"some/path"};
    std::filesystem::remove_all(pathToRemove);

// std::filesystem::rename()

    std::filesystem::path currentName {"some"};
    std::filesystem::path newName {"all"};
    std::filesystem::rename(currentName, newName);

// Utilities

// std::filesystem::file_size()

#include <iostream>
#include <filesystem>

int main()
{
    std::filesystem::path logFilePath {"builddir/meson-logs/meson-log.txt"};
    unsigned long fileSize = std::filesystem::file_size(logFilePath);
    std::cout << "File size: " << fileSize << " bytes" << std::endl;
    return 0;
}


// NEXT TOPIC: streams

// Show: std::ofstream

#include <iostream>
#include <filesystem>
#include <fstream>

int main()
{
    std::filesystem::path outputFilePath{"outputFile.txt"};
    std::ofstream outputStream{outputFilePath};
    outputStream << "This is some text that will be saved in the file" << std::endl;
    return 0;
}

// Show: std::ifstream

#include <iostream>
#include <filesystem>
#include <fstream>

int main()
{
    std::filesystem::path filePath{"outputFile.txt"};
    std::ifstream inputStream{filePath};
    std::string fileContents;
    inputStream >> fileContents;
    std::cout << fileContents << std::endl;
    return 0;
}

// Show: getline() revisited

#include <iostream>
#include <filesystem>
#include <fstream>

int main()
{
    std::filesystem::path filePath{"outputFile.txt"};
    std::ifstream inputStream{filePath};
    std::string fileContents;
    std::getline(inputStream, fileContents);
    std::cout << fileContents << std::endl;
    return 0;
}

// Show: overloading operator<< to output arbitrary data

#include <iostream>

struct Point {
    double x = 0;
    double y = 0;
};

std::ostream& operator<< (std::ostream& stream, Point point) {
    stream << "[" << point.x << ", " << point.y << "]";
    return stream;
}

int main()
{
    Point point;
    std::cout << point << std::endl;
    return 0;
}

// Show: declare friend function to also access private fields

#include <iostream>

class Point {
    double x = 0;
    double y = 0;
    friend std::ostream& operator<< (std::ostream& stream, Point point);
};

std::ostream& operator<< (std::ostream& stream, Point point) {
    stream << "[" << point.x << ", " << point.y << "]";
    return stream;
}

int main()
{
    Point point;
    std::cout << point << std::endl;
    return 0;
}

// Show: overloading operator>> to read arbitrary data

#include <iostream>
#include <fstream>

struct Point {
    double x = 0;
    double y = 0;
    friend std::ostream& operator<< (std::ostream& stream, Point point);
    friend std::istream& operator>> (std::istream& stream, Point& point);
};

std::ostream& operator<< (std::ostream& stream, Point point) {
    stream << point.x << " " << point.y;
    return stream;
}

std::istream& operator>> (std::istream& stream, Point& point) {
    stream >> point.x;
    stream >> point.y;
    return stream;
}

void storePoint(Point point) {
    std::ofstream outputFile{"storage.txt"};
    outputFile << point;
}

Point readPoint() {
    Point anotherPoint;
    std::ifstream inputFile{"storage.txt"};
    inputFile >> anotherPoint;
    return anotherPoint;
}

int main()
{
    Point point{5.0, 20.0};
    storePoint(point);
    Point anotherPoint = readPoint();
    std::cout << "The Point read from the file: " << anotherPoint << std::endl;

    return 0;
}

// Show: stringstream

#include <iostream>
#include <sstream>

struct Point {
    double x = 0;
    double y = 0;
    friend std::ostream& operator<< (std::ostream& stream, Point point);
    friend std::istream& operator>> (std::istream& stream, Point& point);
};

std::ostream& operator<< (std::ostream& stream, Point point) {
    stream << point.x << " " << point.y;
    return stream;
}

std::istream& operator>> (std::istream& stream, Point& point) {
    stream >> point.x;
    stream >> point.y;
    return stream;
}

int main()
{
    std::stringstream outputStream;
    Point point{5, 8};
    outputStream << "String, " << 5 << ", " << point << std::endl;
    std::cout << outputStream.str() << std::endl;
    return 0;
}

// Show (OPTIONAL, if there is time): CSV file

#include <iostream>
#include <filesystem>
#include <fstream>
#include <random>
#include <vector>

int main()
{
	int count = 1000;
    std::vector<double> exampleData1(count);
    std::vector<double> exampleData2(count);
    std::vector<double> exampleData3(count);

    std::random_device device;
    std::default_random_engine engine(device());
    std::uniform_real_distribution dist(0.0, 100.0);

    for(int i = 0; i < count; i++) {
    	exampleData1.at(i) = dist(engine);
    	exampleData2.at(i) = dist(engine);
    	exampleData3.at(i) = dist(engine);
    }

    std::filesystem::path outputFilePath{"outputFile.csv"};
    std::ofstream outputStream{outputFilePath};
    
    outputStream << "ID;Vector 1;Vector 2; Vector3" << std::endl;
    for(int i = 0; i < count; i++) {
    	outputStream << i << ";" << exampleData1.at(i) 
    	                  << ";" << exampleData2.at(i) 
    	                  << ";" << exampleData3.at(i) << std::endl; 
    }

    return 0;
}



// NEXT TOPIC: std::unordered_map / std::map

// Show: declaring a map
// Show: using at()

#include <unordered_map>
#include <string>
#include <iostream>

int main()
{
    std::unordered_map<std::string, int> wordMap{
        {"five", 5},
        {"six", 6},
        {"seven", 7},
        {"eight", 8},
        {"nine", 9}
    };

    std::cout << wordMap.at("five") << std::endl;
    return 0;
}

// Show: using insert()

#include <unordered_map>
#include <string>
#include <iostream>

int main()
{
    std::unordered_map<std::string, int> wordMap{
        {"five", 5},
        {"six", 6},
        {"seven", 7},
        {"eight", 8},
        {"nine", 9}
    };

    std::cout << wordMap.at("five") << std::endl;

    // Leave this line out at first so you can show the exception that gets thrown
    wordMap.insert({"two", 2});
    std::cout << wordMap.at("two") << std::endl;
    return 0;
}

// Show: [] operator and its side effects

#include <unordered_map>
#include <string>
#include <iostream>

int main()
{
    std::unordered_map<std::string, int> wordMap{
        {"five", 5},
        {"six", 6},
        {"seven", 7},
        {"eight", 8},
        {"nine", 9}
    };

    if(wordMap["four"] == 0) {
        std::cout << "What happened here??" << std::endl;
    }
    return 0;
}

// Show: iterating over contents

#include <unordered_map>
#include <string>
#include <iostream>

int main()
{
    std::unordered_map<std::string, int> wordMap{
        {"five", 5},
        {"six", 6},
        {"seven", 7},
        {"eight", 8},
        {"nine", 9}
    };

    if(wordMap["four"] == 0) {
        std::cout << "What happened here??" << std::endl;
    }

    for(const std::pair<std::string, int> entry : wordMap) {
        std::cout << "Entry: " << entry.first << " -> " << entry.second << std::endl;
    }
    return 0;
}

// Show: difference with regular std::map

#include <map>
#include <string>
#include <iostream>

int main()
{
    std::map<std::string, int> wordMap{
        {"five", 5},
        {"six", 6},
        {"seven", 7},
        {"eight", 8},
        {"nine", 9}
    };

    for(const std::pair<std::string, int> entry : wordMap) {
        std::cout << "Entry: " << entry.first << " -> " << entry.second << std::endl;
    }
    return 0;
}