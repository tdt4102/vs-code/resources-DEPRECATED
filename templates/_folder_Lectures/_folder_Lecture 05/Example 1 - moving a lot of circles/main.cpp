#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;
    int count = 5000;
    vector<int> xCoordinates(count);
    vector<int> yCoordinates(count);
    vector<int> directionX(count, 1);
    vector<int> directionY(count, 1);

    random_device device;
    default_random_engine engine(device());
    uniform_int_distribution horizontalDistribution(0, window.width());
    uniform_int_distribution verticalDistribution(0, window.height());

    for(int i = 0; i < count; i++) {
        xCoordinates.at(i) = horizontalDistribution(engine);
        yCoordinates.at(i) = verticalDistribution(engine);
    }

    while(!window.should_close()) {
        for(int i = 0; i < count; i++) {
            xCoordinates.at(i) += directionX.at(i);
            yCoordinates.at(i) += directionY.at(i);

            if(xCoordinates.at(i) > window.width()) {
                directionX.at(i) = -1;
            }
            if(xCoordinates.at(i) < 0) {
                directionX.at(i) = 1;
            }
            if(yCoordinates.at(i) > window.height()) {
                directionY.at(i) = -1;
            }
            if(yCoordinates.at(i) < 0) {
                directionY.at(i) = 1;
            }
            window.draw_circle({xCoordinates.at(i), yCoordinates.at(i)}, 7);
        }
        window.next_frame();
    }
}