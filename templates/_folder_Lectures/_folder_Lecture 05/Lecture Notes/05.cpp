// These are the notes that we use for the live coding parts of the regular lectures
// They are made to help us (the lecturers) remember what to show and talk about.
// Also, importantly, they will not compile!

// ---------------------------------------------------------------------------------

// REPETITION

// Show: definition and declaration

#include "std_lib_facilities.h"
double add(double a, double b);

int main() {
	cout << add(5, 6) << endl;
}

double add(double a, double b) {
	return a + b;
}

// Show: multiple declarations are possible, multiple definitions are not

#include "std_lib_facilities.h"
double add(double a, double b);
double add(double a, double b);
double add(double a, double b);
double add(double a, double b);

int main() {
	cout << add(5, 6) << endl;
}

double add(double a, double b) {
	return a + b;
}
double add(double a, double b) {
	return a + b;
}

// Show: linker error

#include "std_lib_facilities.h"
double add(double a, double b);
double add(double a, double b);
double add(double a, double b);
double add(double a, double b);

int main() {
	cout << add(5, 6) << endl;
}

// Show: draw circle

#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;
    int xCoordinate = 100;
    int yCoordinate = 100;
    
    while(!window.should_close()) {
        window.draw_circle({xCoordinate, yCoordinate}, 7);
        window.next_frame();
    }
}

// Show: place randomly

#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;
    int xCoordinate = 100;
    int yCoordinate = 100;

    random_device device;
    default_random_engine engine(device());
    uniform_int_distribution horizontalDistribution(0, window.width());
    uniform_int_distribution verticalDistribution(0, window.height());

    xCoordinate = horizontalDistribution(engine);
    yCoordinate = verticalDistribution(engine);

    while(!window.should_close()) {
        window.draw_circle({xCoordinate, yCoordinate}, 7);
        window.next_frame();
    }
}

// Show: draw many in random places

#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;
    int count = 5000;
    vector<int> xCoordinates(count);
    vector<int> yCoordinates(count);

    random_device device;
    default_random_engine engine(device());
    uniform_int_distribution horizontalDistribution(0, window.width());
    uniform_int_distribution verticalDistribution(0, window.height());

    for(int i = 0; i < count; i++) {
        xCoordinates.at(i) = horizontalDistribution(engine);
        yCoordinates.at(i) = verticalDistribution(engine);
    }

    while(!window.should_close()) {
        for(int i = 0; i < count; i++) {
            window.draw_circle({xCoordinates.at(i), yCoordinates.at(i)}, 7);
        }
        window.next_frame();
    }
}

// Show: add movement

#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;
    int count = 5000;
    vector<int> xCoordinates(count);
    vector<int> yCoordinates(count);

    random_device device;
    default_random_engine engine(device());
    uniform_int_distribution horizontalDistribution(0, window.width());
    uniform_int_distribution verticalDistribution(0, window.height());

    for(int i = 0; i < count; i++) {
        xCoordinates.at(i) = horizontalDistribution(engine);
        yCoordinates.at(i) = verticalDistribution(engine);
    }

    while(!window.should_close()) {
        for(int i = 0; i < count; i++) {
            xCoordinates.at(i)++;
            yCoordinates.at(i)++;

            window.draw_circle({xCoordinates.at(i), yCoordinates.at(i)}, 7);
        }
        window.next_frame();
    }
}

// Show: wrap around

#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;
    int count = 5000;
    vector<int> xCoordinates(count);
    vector<int> yCoordinates(count);

    random_device device;
    default_random_engine engine(device());
    uniform_int_distribution horizontalDistribution(0, window.width());
    uniform_int_distribution verticalDistribution(0, window.height());

    for(int i = 0; i < count; i++) {
        xCoordinates.at(i) = horizontalDistribution(engine);
        yCoordinates.at(i) = verticalDistribution(engine);
    }

    while(!window.should_close()) {
        for(int i = 0; i < count; i++) {
            xCoordinates.at(i)++;
            yCoordinates.at(i)++;

            if(xCoordinates.at(i) > window.width()) {
                xCoordinates.at(i) = 0;
            }
            if(yCoordinates.at(i) > window.height()) {
                yCoordinates.at(i) = 0;
            }
            window.draw_circle({xCoordinates.at(i), yCoordinates.at(i)}, 7);
        }
        window.next_frame();
    }
}


// Show: convert to multiple circles using vectors

#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;
    int count = 5000;
    vector<int> xCoordinates(count);
    vector<int> yCoordinates(count);
    vector<int> directionX(count, 1);
    vector<int> directionY(count, 1);

    random_device device;
    default_random_engine engine(device());
    uniform_int_distribution horizontalDistribution(0, window.width());
    uniform_int_distribution verticalDistribution(0, window.height());

    for(int i = 0; i < count; i++) {
        xCoordinates.at(i) = horizontalDistribution(engine);
        yCoordinates.at(i) = verticalDistribution(engine);
    }

    while(!window.should_close()) {
        for(int i = 0; i < count; i++) {
            xCoordinates.at(i) += directionX.at(i);
            yCoordinates.at(i) += directionY.at(i);

            if(xCoordinates.at(i) > window.width()) {
                directionX.at(i) = -1;
            }
            if(xCoordinates.at(i) < 0) {
                directionX.at(i) = 1;
            }
            if(yCoordinates.at(i) > window.height()) {
                directionY.at(i) = -1;
            }
            if(yCoordinates.at(i) < 0) {
                directionY.at(i) = 1;
            }
            window.draw_circle({xCoordinates.at(i), yCoordinates.at(i)}, 7);
        }
        window.next_frame();
    }
}



// NEXT TOPIC: THE TERMINAL

// Show: the ls command

ls

// Show: print the current directory

echo "$PWD"

// Show: the cd command

cd <dirname>
cd ..

// Show: use tab completion for a path

cd make/sure/there/are/multiple/directories
cd ../../..

// Show: get back to the home directory
cd 

// USE THE CD COMMAND TO GET BACK TO THE ORIGINAL DIRECTORY

cd <what the current working directory was>

// Show: make directory

mkdir build

// Show: delete directory

rm -rf build // MacOS
rmdir build // Windows

// Show: most tools can tell you how to use them

clang++ --help

man mkdir // MacOS / Linux ONLY!!

// NEXT TOPIC: COMPILING A SINGLE FILE

// Preparation: create a new directory
// Preparation: put a main.cpp file in it with the basic program:

#include "std_lib_facilities.h"

int main() {
	cout << "Hello there!" << endl;
	return 0;
}

// Show: checking that clang is installed

clang++ --version // Should list (MinGW compiled by brecht sanders on Windows)

// Show: try to compile it

clang++ main.cpp -o program.exe // will fail: std_lib_facilities.h not found

// Show: remove std_lib_facilities.h include from main.cpp

int main() {
	cout << "Hello there!" << endl;
	return 0;
}

clang++ main.cpp -o program.exe // cout and endl: not found

// Show: add #include <iostream>

#include <iostream>

int main() {
	cout << "Hello there!" << endl;
	return 0;
}

// main.cpp:3:31: error: use of undeclared identifier 'endl'; did you mean 'std::endl'?
//    cout << "Hello there!" << endl;
//                              ^~~~
//                              std::endl

// Show: change to std::cout and std::endl:

#include <iostream>

int main() {
	std::cout << "Hello there!" << std::endl;
	return 0;
}

// Show: compile program, run program:

clang++ main.cpp -o program.exe

./program.exe

// NEXT TOPIC: NAMESPACES

// Show: declare a function in a namespace

#include <iostream>
namespace German {
	void sayHello() {
		std::cout << "Guten Tag!" << std::endl;
	}
}

int main() {
	German::sayHello();
}

// Show: no longer possible to call function without namespace

int main() {
	sayHello();
}

// Show: nested namespaces

#include <iostream>
namespace English {
	namespace British {
		void sayHello() {
			std::cout << "Good day!" << std::endl;
		}
	}
	namespace US {
		void sayHello() {
			std::cout << "Good day y'all!" << std::endl;
		}
	}
}

int main() {
	English::British::sayHello();
	English::US::sayHello();
}

// NEXT TOPIC: PARTIAL COMPILATION

// Show: linker error when a function is missing

#include "std_lib_facilities.h"
double add(double a, double b);

int main() {
	cout << add(5, 6) << endl;
}

// Show: Open a new folder, create a few .cpp files, and compile them using a single command
// Have each file print out a unique line

clang++ main.cpp file1.cpp file2.cpp file3.cpp -o program.exe

// Show: compile each file separately
// Also show .o files

clang++ -c main.cpp -o main.o
clang++ -c file1.cpp -o file1.o
clang++ -c file2.cpp -o file2.o
clang++ -c file3.cpp -o file3.o

clang++ main.o file1.o file2.o file3.o -o program

// Open the original workspace (use recent folders)

// Show: show GLOB button (C++ menu on the left, top section)

GLOB BUTTON

// Show: can't have multiple definitions of the same function

// File: main.cpp
#include "std_lib_facilities.h"
double add(double a, double b) {
	return a + b;
}

int main() {
	cout << add(5, 6) << endl;
}

// File: add.cpp
double add(double a, double b) {
	return a + b;
}

// Show: basic header file

// File: main.cpp
#include "file2.h"

int main() {
    sayHello();
}

// File: file2.h
#pragma once
void sayHello();

// File: file2.cpp
#include "file2.h"
#include <iostream>
void sayHello() {
    std::cout << "Hello!" << std::endl;
}

// Show: what happens if you include a .cpp file
// File: main.cpp
#include "file2.cpp"

int main() {
    sayHello();
}

// Show: #include straight up copies and pastes

// File: main.cpp
#include <iostream>

int main() {
#include "somefile.txt"
}

// File: somefile.txt
std::cout << "I'm from another planet" << std::endl;

