#include "file1.h"
#include "file2.h"
#include "file3.h"
#include <iostream>


int main() {
	std::cout << "This is main.cpp!" << std::endl;
	functionInFile1();
	functionInFile2();
	functionInFile3();
}