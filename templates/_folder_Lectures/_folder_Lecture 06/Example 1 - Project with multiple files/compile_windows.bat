rmdir -Recurse build
mkdir build

clang++ -c src/main.cpp -o build/main.o
clang++ -c src/file1.cpp -o build/file1.o
clang++ -c src/file2.cpp -o build/file2.o
clang++ -c src/file3.cpp -o build/file3.o

clang++ build/main.o build/file1.o build/file2.o build/file3.o -o build/program.exe