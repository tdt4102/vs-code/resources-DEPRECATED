// These are the notes that we use for the live coding parts of the regular lectures
// They are made to help us (the lecturers) remember what to show and talk about.
// Also, importantly, they will not compile!

// ---------------------------------------------------------------------------------

// REPETITION: COMPILATION

// Show: second file

// File: main.cpp
int main() {
    sayHello();
    return 0;
}

// File: hello.h
#pragma once
void sayHello();

// File: hello.cpp
#include "sayHello.h"
#include <iostream>
void sayHello() {
    std::cout << "hello!" << std::endl;
}

// Show: modify previous to show compilation error and linker error
// Show: what happens if you put definition in header file

// NEXT TOPIC: BUILD SYSTEMS

// Show: create new project > lecture 06 > example 1

// Show: compile using batch script

./compile_windows.bat
build/program.exe
rmdir -Recurse build

// Show: compile using make

mingw32-make 
build/program.exe
mingw32-make clean

// Show: compile using cmake

mkdir build
cd build 
cmake .. -G Ninja
ninja

// Show: compile using meson

cd ..
meson builddir
cd builddir
ninja

// NEXT TOPIC: DEMONSTRATION OF USING WRAPDB

// Show: create an empty directory
// Show: go to wrapdb (link in slides), fetch the .wrap for tabulate.
//       Save the file in a new directory called "subprojects"
// Show: create a main.cpp file with a main function

#include <iostream>
int main() {
    std::cout << "Hello there!" << std::endl;
}

// Show: write a meson file

project('testproject', 'cpp',
    version : '0.1',
    default_options : ['warning_level=2', 'cpp_std=c++17'])

tabulate_dep = subproject('tabulate').get_variable('tabulate_dep')

exe = executable('program', 'main.cpp', dependencies : [tabulate_dep])

// Show: configure project

meson setup builddir

// Show: use the tabulate library

#include <string>
#include <iostream>
#include <tabulate/table.hpp>

int main() {
    tabulate::Table table;
    table.add_row({"Heading 1", "Heading 2", "Heading 3"});
    table.add_row({"Item 1", "Item 2", "Item 3"});
    table.add_row({"Item 1", "Item 2", "Item 3"});
    table.add_row({"Item 1", "Item 2", "Item 3"});
    table.add_row({"Item 1", "Item 2", "Item 3"});
    table.add_row({"Item 1", "Item 2", "Item 3"});
    table.add_row({"Item 1", "Item 2", "Item 3"});
    table.add_row({"Item 1", "Item 2", "Item 3"});

    table.row(0).format().font_style({tabulate::FontStyle::bold});

    std::cout << table << std::endl;

    return 0;
}



// NEXT TOPIC: STRUCTS

// Show: basic struct  
//     Best practice: define a struct in its own header file
struct Point {
    double x = 0;
    double y = 0;
};

// Show: reading and writing the values of a struct
struct Point {
    double x = 0;
    double y = 0;
};

int main() {
    Point location;
    location.x = 5;
    return 0;
}

// Show: can't use cout with structs
struct Point {
    double x = 0;
    double y = 0;
};

int main() {
    Point location;
    location.x = 5;
    cout << location << endl;
    return 0;
}

// Show: initialising the values of a struct
struct Point {
    double x = 0;
    double y = 0;
};

int main() {
    // Either: use default values
    Point location;
    // Or: assign values
    location.x = 5;
    location.y = 2;
    // Or: use {} syntax
    Point anotherLocation {5, 6};
    cout << location << endl;
    return 0;
}

// Show: vector of struct
struct Point {
    double x = 0;
    double y = 0;
};

int main() {
    std::vector<Point> points(100, {0, 0});
    return 0;
}

// Show: struct in struct
struct Date {
    int year = 0;
    int month = 0;
    int day = 0;
};

struct Time {
    int hour = 0;
    int minutes = 0;
};

struct Datetime {
    Date date;
    Time time;
};

int main() {
    // initialisation
    Datetime today {{2022, 01, 30}, {10, 30}};
    today.time.hour = 11;
}

// Show: struct with a bunch of different data types
struct Book {
    std::string title;
    std::vector<Author> authors;
    int pageCount = 0;
    int releaseYear = 0;
    Language language;
};

// Show: can't define a struct twice
// File "point.h"
struct Point {
    double x = 0;
    double y = 0;
};

// File: origin.h
#pragma once
#include "point.h"
Point getOrigin();

// File: "origin.cpp"
#include "origin.h"
Point getOrigin() {
    Point origin {0, 0};
    return origin;
}

// File: main.cpp
#include "point.h"
#include "origin.h"
int main() {
    Point origin = getOrigin();
    return 0;
}

// Show: assignment copies values
// ! SHOW IN DEBUGGER
struct Point {
    int x = 0;
    int y = 0;
};

int main() {
    Point pointA {1, 2};
    Point pointB {5, 6};
    pointA = pointB;
}

// Show: initialise values of one struct with those of another
// ! SHOW IN DEBUGGER
struct Point {
    int x = 0;
    int y = 0;
};

int main() {
    Point pointA {1, 2};
    Point pointB {pointA};
}


// NEXT TOPIC: CONST

// Show: const variable

const double pi = 3.1415927;
const double radiansPerDegree = (2.0 * pi) / 360.0;
const int maxTreesInLandscape = 3;

// Show: can't update value after the initial one

const int count = 10;
count++; // error

// Show: can be used in any place a data type is used
int main() {
    std::vector<const int> test;
    test.push_back(1);
    test.at(0) = 5; // error
}

// Show: const struct means you can't modify fields
struct Point {
    int x = 0;
    int y = 0;
};

int main() {
    const Point pointA {50, 5};
    pointA.x = 6; // error
    return 0;
}

// Show: const field in struct
struct Point {
    int x = 0;
    const int y = 0;
};

int main() {
    Point point;
    point.x = 6;
    point.y = 3; // error
    return 0;
}

// Show: const fields in struct means you cannot assign in vector
struct Point {
    int x = 0;
    const int y = 0;
};

int main() {
    std::vector<Point> points;
    Point point {1, 2};
    Point anotherPoint {1, 2};
    points.push_back(point);
    points.at(0) = anotherPoint;
    return 0;
}




// NEXT TOPIC: REFERENCES

// Show: value being copied
int main() {
    int value = 5;
    int anotherValue = value;
    anotherValue = 10;
    cout << value << endl;
}

// Show: basic reference usage
int main() {
    int value = 5;
    int& anotherValue = value;
    anotherValue = 10;
    cout << value << endl;

    value = 20;
    cout << anotherValue << endl;
}

// Show: assigning a reference to one another causes value assignment
int main() {
    int value = 5;
    int& reference = value;
    int& anotherReference = reference;

    anotherReference = 30;
    cout << value << endl;
}

// Show: a reference must always refer to something
int main() {
    int value = 5;
    int& reference; // error
    return 0;
}

// Show: data type of referenced value must match exactly
int main() {
    int integer = 4;
    int& referenceToInteger = integer;
    double& referenceToFloating = referenceToInteger; // error
}


// Show: why use references at all?
//       Reason 1: use function that modify a variable
struct Mug {
    string contents;
}

void coffeeMachine(Mug &mugToFill) {
    mugToFill.contents = "coffee";
}

// Show: why use references at all?
//       Reason 2: multiple output values
void readSensors(double &temperature, double &humidity) {
    temperature = 10;
    humidity = 1;
}

int main() {
    double temperature;
    double humidity;
    readSensors(temperature, humidity);
    // use values here
    return 0;
}

// Show: why use references at all?
//       Reason 3: avoid copies
// ! SHOW IN DEBUGGER
// ! SHOW MEMORY USAGE IN TASK MANAGER
// Turn parameter into reference and run again
void doStuff(vector<int> someVector) {
    for(long long i = 0; i < someVector.size(); i++) {
        someVector.at(i) = 5;
    }
}

int main() {
    const long long manyIntegers = 1024 * 1024 * 1024;
    vector<int> bigVector(manyIntegers);
    doStuff(bigVector);

    return 0;
}

// Show: reference to struct
// ! USE DEBUGGER
struct Point {
    int x = 0;
    int y = 0;
}

void moveToOrigin(Point &point) {
    point.x = 0;
    point.y = 0;
}

int main() {
    Point point {1, 2};
    moveToOrigin(point);
}

// Show: const reference
struct Point {
    int x = 0;
    int y = 0;
}

void printPoint(const Point &point) {
    point.x = 0; // error
    std::cout << "(" << point.x << ", " << point.y << ")" << std::endl;
}

int main() {
    Point point {1, 2};
    printPoint(point);
} 

// Show: what does this code print out?
#include <iostream>

void modify(int value) {
    value += 2;
}

int main() {
    int number = 3;
    modify(number);
    std::cout << "The value of number is: " << number << std::endl;
    return 0;
}

// Show: what does this code print out?
#include <iostream>

int modify(int& value) {
    value += 2;
    return value;
}

int main() {
    int number = 3;
    number = modify(number);
    std::cout << "The value of number is: " << number << std::endl;
    return 0;
}
