// Function Overloading

// Function overloading is declaring multiple functions with the same name but different parameters. This can be useful when you want to allow .

// When you overload functions, you should make sure that all functions do the same thing, just with different parameter types.

// Also, C++ does not allow you to have two functions with the same name, same parameters, but only different return types.


// These two functions have the same name, but different parameter types. They also do the same thing.
int add(int a, int b) {
	return a + b;
}

double add(double a, double b) {
	return a + b;
}

int main() {
	cout << "5 + 6 = " << add(5, 6) << endl;
	cout << "5.0 + 6.0 = " << add(5.0, 6.0) << endl;
	// This is not allowed, because we provide a parameter of type double (5.0), and one of type int (6). Now the compiler does not know which of the two functions to use because it can convert either of the two values to make it work with either of the two variants of add()
	cout << "5.0 + 6 = " << add(5.0, 6) << endl;
	return 0;
}