// These are the notes that we use for the live coding parts of the regular lectures
// They are made to help us (the lecturers) remember what to show and talk about.
// Also, importantly, they will not compile!

// ---------------------------------------------------------------------------------

// REPETITION FROM LAST TIME

// Show: data types and variable declarations
bool isTrue = true;

// Show: precision errors
double doubleValue = 12550224.8;
float floatValue = doubleValue;
double convertedBack = floatValue;
// ! USE DEBUGGER, cout truncates number

// Show: Type conversion can result in weird behaviour when the type is too small
int intValue = 500;
char charValue = intValue;
int convertedBack = charValue;
cout << "The 500 ended up being: " << convertedBack << endl;
// prints -12
// Conversion only makes sense if the value "fits" in the type

// Show: This is how we cast, but it does not change how the conversion is done
int intValue = 500;
char charValue = static_cast<char>(intValue);
int convertedBack = charValue;
cout << "The 500 ended up being: " << convertedBack << endl;
// Compiler warning is also gone now!

// Show: for loop using a debugger to show the order of operations
for(int i = 0; i < 10; i += 2) {
	cout << "The value of i is now: " << i << endl;
}

// Show: for loop can start at a nonzero value
int startIndex = 5;
for(int i = startIndex; i < 10; i += 2) {
	cout << "The value of i is now: " << i << endl;
}

// Show: basic function syntax
double halve(double number) {
	return number / 2;
}

int main() {
	double half = halve(19);
	cout << half << endl;
	return 0;
}

// Show: return value must be of the declared type
double halve(double number) {
	return "I have halved the number!";
}

// Show: missing return statement causes crash
double halve(double number) {
	
}

int main() {
	double half = halve(19);
	cout << half << endl;
	return 0;
}

// Show: void functions don't return a value
void printHalf(double number) {
	cout << number / 2 << endl;
}

int main() {
	cout << printHalf(19) << endl;
	return 0;
}

// Show: the order in which functions are defined matters
int main() {
	cout << add(5, 6) << endl;
}

int add(int a, int b) {
	return a + b;
}

// Show: declaration is necessary sometimes
int add(int a, int b);

int main() {
	cout << add(5, 6) << endl;
}

int add(int a, int b) {
	return a + b;
}

// Show: declaration without implementation (does not compile)
int add(int a, int b);

int main() {
	cout << add(5, 6) << endl;
}

// Show: function calling itself
void doNothing() {
	doNothing();
}

int main() {
	doNothing();
}

// Show: cin "buffers" words you put into it
string word;
cin >> word;
cout << "First word: " << word << endl;
cin >> word;
cout << "Second word: " << word << endl;
cin >> word;
cout << "Third word: " << word << endl;
cin >> word;
cout << "Fourth word: " << word << endl;

// When running, insert "      A       B       C   "
// Show that the first three are printed immediately,
// program will wait for the fourth word 


// Show create a calculator using the same principle as the previous example
// Start with the main function, show that you can imagine a function and
// implement it later.

double readNumber() {
	double number;
	cin >> number;
	return number;
}

string readOperation() {
	string operation;
	cin >> operation;
	return operation;
}

int main() {
	double a = readNumber();
	string operation = readOperation();
	double b = readNumber();

	if(operation == "+") {
		cout << a << " + " << b << " = " << a+b << endl;
	} else if(operation == "-") {
		cout << a << " - " << b << " = " << a-b << endl;
	} else if(operation == "*") {
		cout << a << " * " << b << " = " << a*b << endl;
	} else if(operation == "/") {
		cout << a << " / " << b << " = " << a/b << endl;
	} else {
		cout << "I do not know the operation: " << operation << endl;
	}
}

// NEXT TOPIC: MORE ON FUNCTIONS

// Show: default parameter values
int increment(int value, int incrementBy = 1) {
	return value + incrementBy;
}

int main() {
	cout << increment(10) << endl;
	cout << increment(10, 5) << endl;
	return 0;
}

// Show: function overloading (last line in main gives problems)
// Can use debugger to show which function is running
int add(int a, int b) {
	return a + b;
}

double add(double a, double b) {
	return a + b;
}

int main() {
	cout << "5 + 6 = " << add(5, 6) << endl;
	cout << "5.0 + 6.0 = " << add(5.0, 6.0) << endl;
	cout << "5.0 + 6.0 = " << add(5.0, 6) << endl;
	return 0;
}

// Show: function overloading can be quite picky 
// (even though we use floating point numbers, compilation still fails)
int add(int a, int b) {
	return a + b;
}

float add(float a, float b) {
	return a + b;
}

int main() {
	cout << "5 + 6 = " << add(5, 6) << endl;
	cout << "5.0 + 6.0 = " << add(5.0, 6.0) << endl;
	return 0;
}

// NEXT TOPIC: STRINGS

// Show: converting strings to numbers
string value = "20.6";
int intValue = stoi(value);
double doubleValue = stod(value);

// Show: various properties (also shown on slide, may be easier)
// No need to initialise
string text;

// Can append char
text += 'H';
text += 'e';
text += 'y';

// Can append string
text += " there!";

// Use \n for a new line
text += "\n";

// Convert values to string before appending
text += "Here is a number: " + to_string(42);

// Print like any other value
cout << text << endl;


// Show: issues with norwegian characters on windows
cout << "Går det an å skrive ut Norske tegn?" << endl;

// Show: C++ considers characters to be numbers
char letterA = 'A';
cout << "The letter " << letterA << " is number: " << int(letterA) << endl;

// NEXT TOPIC: GRAPHICS

// Show: drawing circles
#include "std_lib_facilities.h"
#include "AnimationWindow.h"

int main() {
    AnimationWindow window;

    window.draw_circle({100, 100}, 50);

    window.wait_for_close();
    return 0;
}

// Show: change position and radius
int main() {
    AnimationWindow window;

    window.draw_circle({300, 200}, 150);

    window.wait_for_close();
    return 0;
}

// Show: drawing rectangles
int main() {
    AnimationWindow window;

    window.draw_rectangle({100, 100}, 300, 160);

    window.wait_for_close();
    return 0;
}

// Show: drawing order matters
// Swap the lines to show opposite order
int main() {
    AnimationWindow window;

    window.draw_rectangle({100, 100}, 300, 160);
    window.draw_circle({300, 200}, 120);

    window.wait_for_close();
    return 0;
}

// Show: draw quads (deformable rectangles)
int main() {
    AnimationWindow window;

    window.draw_quad({200, 100}, {100, 200}, {200, 300}, {300, 200});

    window.wait_for_close();
    return 0;
}

// Show: draw triangles
int main() {
    AnimationWindow window;

    window.draw_triangle({350, 120}, {100, 200}, {270, 300});

    window.wait_for_close();
    return 0;
}

// Show: draw line
int main() {
    AnimationWindow window;

    window.draw_line({100, 100}, {200, 300});

    window.wait_for_close();
    return 0;
}

// Show: draw arc
int main() {
    AnimationWindow window;

    window.draw_arc({250, 200}, 150, 150, 30, 200);

    window.wait_for_close();
    return 0;
}

// Show: draw text
int main() {
    AnimationWindow window;

    window.draw_text({100, 100}, "Hello there!");

    window.wait_for_close();
    return 0;
}

// Show: lots of optional parameters available
int main() {
    AnimationWindow window;

    window.draw_text({100, 100}, "It works!", Color::blue, 80,
                     Font::courier_bold);

    window.wait_for_close();
    return 0;
}

// Show: drawing a lot of things
int main()
{
	AnimationWindow window;

	for(int i = 0; i < 1000; i+=30) {
		window.draw_circle({i, 100}, i/30);
	}
	
	window.wait_for_close();
	return 0;
}