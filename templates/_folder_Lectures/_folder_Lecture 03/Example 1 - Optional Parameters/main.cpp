// Default parameters

// Only needed in the declaration
// Must be last parameter
// Possible to have all parameters optional
// Not possible to "skip" any

// For some functions there are parameter values that you don't necessarily care 
// about when you call that function. In that case it is possible to create a
// parameter that is optional. If you call that function without specifying a
// value for that parameter, the default value will be used instead.

// Here's an example of what that looks like:

void sayHello(string message = "Hello there!") {
	cout << message << endl;
}

void greet() {
	// Prints "Greetings!"
	sayHello("Greetings!");

	// Prints "Hello there!"
	sayHello();
}

// It is possible to have any number of default parameters, but there are
// some restrictions. 

// ## Restriction 1:
// All default parameters must be at the end of the parameter list

// As such this is not allowed:

// int increment(int incrementBy = 1, int value)

// But this is:
int increment(int value, int incrementBy = 1) {
	return value + 1;
}

// Why? Let's look at a function with multiple optional parameters:

int optionalParameters(int x = 0, int y = 0) {
	return x * y;
}

// If you had called this function like this:

// optionalParameters(5);

// The compiler can't figure out if you are specifying the value for 
// parameter x or y. And unlike Python, it is not possible to specify
// which parameter value you are specifying. So:

// optionalParameters(y = 5);

// Is not allowed in C++. For this reason, all optional parameters must
// be at the end of the list.

// ## Restriction 2: 
// If a function has multiple optional parameters, to specify the value
// of a later parameter, you must also specify all parameters that
// come before it in the parameter list. 
// For example, using the optionalParameters function above,
// you can only specify y if you also specify x.



