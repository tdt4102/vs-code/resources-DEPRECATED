// These are the notes that we use for the live coding parts of the regular lectures
// They are made to help us (the lecturers) remember what to show and talk about.
// Also, importantly, they will not compile!

// ---------------------------------------------------------------------------------

// REPETITION: Struct

// Show: declaration in header file

// File: counter.h
#pragma once
struct Counter {
	int value = 0;
};

// File: main.cpp
#include "counter.h"

int main() {
	Counter counter;
	for(int i = 0; i < 10; i++) {
		counter.value++;
	}
	std::cout << "After incrementing: " << counter.value << std::endl;
}

// REPETITION: const
const int value = 10;
value = 45; //not allowed

// REPETITION: references
double number = 5;
double& reference = number;
reference = 10;
std::cout << number << std::endl;

const double& otherReference = number;
otherReference = 11; //not allowed

// ----------------------------------------------------

// NEXT TOPIC: Object-Oriented Programming

// Show: declaring and defining methods
// Show (also): using fields inside methods

// File: counter.h
struct Counter { // <- struct and not class!
	int value = 0;

	void increment();
	void reset();
	int get();
};

// File: counter.cpp
#include "counter.h"

void Counter::increment() {
	value++;
}

void Counter::reset() {
	value = 0;
}

int Counter::get() {
	return value;
}

// File: main.cpp
#include "counter.h"
#include <iostream>

int main() {
	Counter counter;
	for(int i = 0; i < 10; i++) {
		counter.increment();
	}
	std::cout << "After incrementing: " << counter.get() << std::endl;
	counter.reset();
	std::cout << "After reset: " << counter.get() << std::endl;
}


// Show: private by default
// File: counter.h
class Counter {
	int value = 0;

	void increment();
	void reset();
	int get();
};


// Show: public, private, and protected regions
// File: counter.h
class Counter {
private:
	int value = 0;
public:
	void increment();
	void reset();
	int get();
};


// Show: constructor. Create constructor first, let compilation fail
//       Then change visibility of constructor
//       Then call constructor in main.cpp
// File: counter.h
class Counter {
private:
	int value = 0;
	Counter(int startValue);
public:
	void increment();
	void reset();
	int get();
};

// File: counter.cpp
#include "counter.h"

Counter::Counter(int startValue) {
	value = startValue;
}
/* ... */

// File: main.cpp
#include "counter.h"
#include <iostream>

int main() {
	Counter counter(30);
/* ... */
}


// Show: default constructor
// File: counter.h
class Counter {
private:
	int value = 0;
public:
	Counter(int startValue);
	Counter() = default;
	void increment();
	void reset();
	int get();
};


// Show: constructor and {} syntax
Counter::Counter(int startValue)
	: value{startValue} {
}


// Show: constructor initialisation syntax (with const)
// File: counter.h
class Counter {
private:
	int value = 0;
	const int maxValue = 0;
public:
	Counter(int startValue, int maxValue = 5000);
/* ... */
};

// File: counter.cpp
#include "counter.h"

Counter::Counter(int startValue, int max)
	: maxValue{max} {
	value = startValue;
	// maxValue = max; <- show this first, compilation fails
}


// Show: constructors of std::vector and std::string
#include <string>
#include <iostream>
#include <vector>

int main() {
	std::string text("Strings have constructors!");
	std::cout << text << std::endl;
	std::vector<int> list(10);
}


// Show: AnimationWindow.h file
// Hey look, it's a class!


// NEXT TOPIC: Enum

// Show: why enum class and not enum
enum CarColour {
    BLUE, BLACK, RED	
};
enum SweaterColour {
    GREEN, YELLOW, RED
};


void useColour(CarColour colour) {}

int main() {
    useColour(GREEN);
    return 0;
}


// Show: basic usage of enum class
// File: color.h
enum class Color {
    red, green, blue, black
};

// File: main.cpp
#include "color.h"

int main() {
	Color colour = Color::blue;
	return 0;
}


// Show: can't use cout (to print a legible name)
// File: main.cpp
#include "color.h"

int main() {
	Color colour = Color::blue;
	std::cout << colour << std::endl;
	return 0;
}


// Show: convert to and from int, int used under the hood
// File: color.h
enum class Color {
    red = 5, green, blue, black
};

// File: main.cpp
#include "color.h"

int main() {
	Color colour = Color::blue;
	std::cout << static_cast<int>(colour) << std::endl;
	Color anotherColour = static_cast<Color>(6);
	return 0;
}



// Show: switch case and enums
// File: color.h
enum class Color {
    red, green, blue, black
};

void printColor(Color color);

// File: color.cpp
#include "color.h"
#include <iostream>

void printColor(Color color) {
	switch(color) {
		case Color::red:
			std::cout << "red";
			break;
		case Color::green:
			std::cout << "green";
			break;
		case Color::blue:
			std::cout << "blue";
			break;
		case Color::black:
			std::cout << "black";
			break;
	}
}

// File: main.cpp
#include "color.h"

int main() {
	Color colour = Color::blue;
	std::cout << "The value of colour is: ";
	printColor(colour);
	std::cout << std::endl;
	return 0;
}



// Show: overriding int values




