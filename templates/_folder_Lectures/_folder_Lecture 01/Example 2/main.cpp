// Lecture 1, Example 2 - Bart Iver van Blokland

#include "std_lib_facilities.h"

int main() {
    cout << "A very Happy New Year!" << endl;
    cout << "Hope everyone had a nice break =)" << endl;
    cout << "Did santa bring you anything nice?" << endl;
    cout << "I'm just stalling for time.." << endl;
    cout << ".. so that we can try the debugger!" << endl;

    return 0;
}