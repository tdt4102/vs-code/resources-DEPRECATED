// These are the notes that we use for the live coding parts of the regular lectures
// They are made to help us (the lecturers) remember what to show and talk about.
// Also, importantly, they will not compile!

// ---------------------------------------------------------------------------------

// SLIDE: We are now starting to talk about everything discussed in assignment 1
// SLIDE: We are first going to demonstrate a bunch of stuff, 
//        then give you a summary on the slides afterwards


// Starting point: blank project
// 1. Compile and run, show where the output is placed
// 2. Create a new project in a directory with ø, æ, or å on Windows
//        Intent: show that this does not work
//        Windows gets funny with unicode

// 3. Note the Meson execution:
//     - Should show the end summary if it executed correctly
//     - Meson tends to write all kinds of scary things that are missing.
//       Remember to go through everything and show what stuff in the meson log means

// 4. Try to compile the empty file
//        This works in Python, but not in C++
// 5. Not possible to just write print()
// 6. C++ programs do not start to execute from the top
// 7. Write:

int main() {

}

// 8. Things to observe here:
//     - This compiles!
//     - Everything in between the {} braces is what the program will run
//     - The int main() bit has meaning. You cannot change any of these. 
//     - We'll explain more what all of it means in later lectures
//     - The program will begin to run here

// 9. Add the line:

cout << "Message!" << endl;

// 10. The "cout <<" means "push the text into cout, which then appears on the terminal"
//     - The endl bit means END Line. Also ensures that the line is shown on screen
//     - Note that there is a ; at the end of the line

// 11. Compile the project, it will fail (anyone remember what also used to be here before?)
// 12. To fix, add the line:

#include "std_lib_facilities.h"

// 13. Compile the project.
//     - It should work now, but why?
//     - Just like in Python, not all functionality is always available
//     - We need to "import" certain things
//     - The C++ equivalent is "#include"
//     - Has to be at the top of file. Move it below main() to show
//     - _unlike_ Python, the equivalent to print() is not available by default
//     - Unlike Python, the print() equivalent in C++ is not a function.
//     - It is possible to use '' for strings in Python, not in C++
//     - Don't add newlines in a string -> syntax error!
//     - Don't use Norwegian characters on Windows
//     - You should use the "endl", show what happens when removing it
//         - a) Output might not always get shown
//         - b) Next line is not shown on a fresh line

// 14. Move the line outside the function
//     - Compiler complains. 
//     - Code must be inside the {} braces

// 14. Back to the ;
//     - In Python, starting a new line of code marks the end of a "sentence"
//     - In C++ you can do what you like
//         - Insert newlines between each part of the cout line and compile
//         - Also mess with the whitespace (extra indents and such)
//         - Can't insert whitespace in between "meaningful bits", such as "cout"

// 15. It is possible to chain cout values
//     - Write some, also on multiple lines, possibly separated by endl's

// 16. We can also print numbers
//     - We will come back to working with numbers in the next lecture
//     - Print an integer and a floating point number
//     - When hardcoding values, cout << "It is the year 2023" 
//                           and cout << "It is the year " << 2023 are the same thing.

// 17. Debugger time
//     - You should have 3-4 lines at this point in the program
//     - Create a breakpoint in the left margin
//     - Show what happens when you run with debugging
//     - Show what happens when you run WITHOUT debugging
//     - Show what happens when you move the breakpoint to further up ahead
//     - Only use "step over" for now! Step into and step out are not yet relevant
//     - Show that the debugger will execute one line at a time, even when 
//       that line covers multiple lines in the file
