#include <iostream>

class Satellite {
public:
    std::string name;
    Satellite(std::string _name) : name{_name} {}
};

class Houston {
    Satellite* satellite;
public:
    Houston() {
        satellite = new Satellite("sputnik");
    }
    Houston(const Houston& other) {
        std::cout << "Houston has been duplicated!" << std::endl;
        std::cout << "What kind of messed plot line is this??" << std::endl;
        satellite = new Satellite(other.satellite->name);
    }
    ~Houston() {
        delete satellite;
        std::cout << "Satellite deorbit completed successfully!" << std::endl;
    }
};

int main() {
    Houston houston1;
    Houston houston2 = houston1;
}