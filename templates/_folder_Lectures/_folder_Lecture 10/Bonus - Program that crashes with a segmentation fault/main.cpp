#include <vector>

int main() {
	std::vector<int> tinyVector;
	// We use the [] operator here to attempt to write a value that's well outside the
	// memory region in which the values of the vector reside.

	// This causes the program to crash with a segmentation fault (SIGSEGV) on MacOS/Linux, or 
	// Access Violation on Windows.

	// If we were to use the at() function here, we would get an out of range error instead,
	// which we for the purposes of this demonstration do not want.
	tinyVector[1000] = 42;
	return 0;
}