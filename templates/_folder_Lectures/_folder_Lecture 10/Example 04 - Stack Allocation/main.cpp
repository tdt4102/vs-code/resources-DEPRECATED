#include <iostream>
#include <vector>
#include <array>

// All variables that are not allocated using the new operator are stored on the stack
// When the scope in which the variable is declared exits, the memory associated with that
// variable is deallocated to that variable automatically

// We recommend you step through this program from the first line of main(), and look in the Run and Debug
// tab on the left hand side and see when different variables are allocated and deallocated in the 
// variables list. Also take a look at the "call stack" subdivision below the variables list.

void functionA();
void functionB();

void functionA() { // x and y are allocated here
	float x;
	functionB();
	double y;
} // x and y are deallocated here

void functionB() { // list and d are allocated here
	int d = 15;
	std::array<int, 10> list = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	if(list.at(0) == 0) { // j is allocated here
		int j = 10;
	} // j is deallocated here
} // list and d are deallocated here


int main() { // a is allocated here
	int a = 1;
	functionA();
} // a is deleted here