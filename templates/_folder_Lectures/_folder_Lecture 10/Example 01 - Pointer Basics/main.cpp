#include <vector>
#include <iostream>
#include <string>

int main() {
	// A pointer is declared by specifying any data type, and appending *
	// This indicates the variable contains a memory address,
	// and where the memory address being referenced contains a value of the
	// specified data type. For example:
	float* floatPointer = nullptr;
	// The variable floatPointer contains a memory address, because its
	// type has a * appended to it, indicating it is a pointer.
	// This pointer is intended to reference a value of type float.

	// There are no constraints to which data types your pointer can point at
	// Here are a few examples:
	int* integerPointer = nullptr;
	std::string* pointerToString = nullptr;
	std::vector<int>* pointerToVectorOfString = nullptr;

	// You may have noticed that the above pointers were initialised to the value nullptr
	// nullptr is a special address that is always invalid (usually the address 0, hence nullptr)
	// Just like any other variables, pointers are not initialised 
	// automatically when you declare them, so always assigning them the value nullptr
	// is a good habit to have.

	// Since pointers store a memory address, we need to be able to get the memory address
	// of values somehow. That's where the & operator comes in:
	std::string helloMessage = "Hello!";
	std::string* message = &helloMessage;

	// Now the address of the 'helloMessage' variable is stored in the pointer 'message'.

	// If we want to use the value stored at the address referenced by a pointer,
	// we need to 'dereference' it. This is done using the * operator:
	*message += " And good day to you!";

	// After we dereference the pointer, we can both modify the value and read its value.
	// Here's two examples where we dereference a pointer to read its value:
	std::cout << *message << std::endl;
	std::string newMessage = *message;

	// Since we have used the pointer to modify the value it references, specifically
	// helloMessage, that value has now changed:
	std::cout << helloMessage << std::endl;

	// If we did not dereference the pointer, and instead printed the pointer directly,
	// we would see the memory address stored in the pointer:
	std::cout << "Message references the address: " << message << std::endl;

	// Performing arithmetic operations on a pointer modifies the address it references:
	message++;
	std::cout << "Address after incrementing it: " << message << std::endl;

	// Resetting the address so it points to helloMessage again
	message--;

	// If you want to avoid accidentally changing the address referenced by a pointer
	// (for example if you forgot to dereference it when modifying a value)
	// we recommend that you declare its address to be const
	// If you don't intend to modify its address anyway (you usually don't),
	// the compiler will prevent you from making that mistake :)
	// NOTE: the const needs to be placed AFTER the pointer type:
	int number = 10;
	int* const pointerToNumber = &number;
	// pointerToNumber += 25; // error: tried to change address
	*pointerToNumber += 25; // all good! Incrementing number by 25

	// When you want to access members (fields or methods) of an object referenced by a pointer,
	// you can either dereference the pointer first, then use the members normally,
	// or you can use the handy shorthand -> operator.
	// The following lines are completely the same, just syntactically different:
	std::cout << "Number of characters: " << message->length() << std::endl;
	std::cout << "Number of characters: " << (*message).length() << std::endl;

	return 0;
}