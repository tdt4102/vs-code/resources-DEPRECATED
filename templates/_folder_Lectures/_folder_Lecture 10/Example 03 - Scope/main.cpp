#include <iostream>

// The rules for scope:
// - A name exists between the point it is declared within a scope {} and the end of that scope
// - A name also exists in scopes declared within the scope {}

// Let's look at an example with a function:
int main() { // this is the start of the scope of the main function
	
	// a does not exist here
	// b does not exist here
	
	int a = 0;

	// a exists here
	// b does not exist here

	if(a == 0) { // here starts a nested scope within the scope of main()
		// a exists here
		// b does not exist here

		int b = a; // b can use the value of a, because a exists here

		// a exists here
		// b exists here

	} // The containing scope of b ends here, which therefore ceases to exist at this point, and can not be used beyond this point

	// a exists here
	// b does not exist here

	return 0;
} // The containins scope of a ends here, which ceases to exist at this point.




// Now for another example: namespaces
// While namespaces, classes, etc. all _technically_ have a scope (as shown below),
// they behave a bit different than block scopes such as those of if statements, for loops, and so on.
// Primarily in that the names declared within them stay around after their scope closes.

namespace Tools { // The scope of Tools starts here
	// Wrench does not exist here

	class Wrench {

	};

	// Wrench exists here
}

// Wrench still exists, but is located inside the scope of the Tools namespace
// We need to use the scope operator to reference it.
void useWrench(Tools::Wrench wrench) {

}