#include <iostream>

int main() {
	// Let's try to see how we can cause chaos with heap memory

	// This memory leak will occupy around 4GiB of RAM
	// Since it throws the pointers to that memory away,
	// we cannot use delete anymore to deallocate it.
	// That is a memory leak
	for(int i = 0; i < 1024*1024; i++) {
		int* fourKiloBytes = new int[1024];
		std::cout << fourKiloBytes << std::endl;
	}

	std::string* intoTheVoid = new std::string("void");
	delete intoTheVoid;
	// intoTheVoid is now a dangling reference
	// We have deleted the memory it points to
	// But since it still contains the address of the memory it pointed to previously,
	// we could still try to use it if we wanted to (but doing so will crash the program)

	// And finally, a double free. That is trying to delete a value that was already deleted previously
	double* doubleValue = new double{5.0};
	delete doubleValue;
	delete doubleValue; // Error: doubleValue was already deleted
}