#include <iostream>
#include <vector>

int& createInvalidReference() {
	int number = 10;
	return number;
}

int main() {
	std::string text = "Hello there :)";


	// 1. References another value
	// Yes on both, but the way we create that reference is slightly different
	std::string* pointerToText = &text;
	std::string& referenceToText = text;


	// 2. Can reference a value that does not exist
	// A pointer can be set to nullptr, which is an address that is always invalid:
	int* pointerToNothing = nullptr;
	int ohDear = *pointerToNothing; // error: the address of nullptr is invalid!

	// References are harder to use incorrectly, but one example of where
	// you could accidentally create a reference that refers to a value that no longer exists
	// For example, after createInvalidReference() exits, the local variable 'number' is deleted
	// The reference will still reference the memory where it used to be
	int& invalidReference = createInvalidReference();
	// For technical reasons this will still print 10!
	std::cout << invalidReference << std::endl; 


	// 3. Can be set to nullptr
	// Let's see what happens when we try to set a reference to the value nullptr
	double PI = 3.1415927;
	double& referenceToPI = PI;

	// Assigning to a reference overwrites the value being referenced
	// We cannot get to the memory address
	referenceToPI = 10;

	double* pointerToPI = &PI;
	// Assigning to a pointer changes its address.
	// It can therefore be set to nullptr after it is created
	pointerToPI = nullptr;


	// 4. Can modify the address being referenced
	// See the lines under point 3.


	// 5. Possible to create a vector or array containing pointers or references
	std::vector<int*> thisIsPossible;
	// std::vector<int&> thisIsNotPossible; // this spews out 17 errors on my machine.


	// 6. Need to dereference explicitly
	referenceToPI = 10; // the reference is dereferenced automatically
	*pointerToText = "hello"; // Pointers need to be dereferenced to read or modify the value referenced by them
	
}