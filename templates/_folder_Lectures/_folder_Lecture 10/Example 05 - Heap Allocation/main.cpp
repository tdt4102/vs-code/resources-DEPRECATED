#include <string>
#include <iostream>
#include <vector>

std::string* functionWhichAllocatesAString() {
	// The new operator allows us to allocate a value of any data type on the heap
	int* intValue = new int {4};
	double* doubleValue = new double {3.0};
	// I hope you never need this data type, but I did say _any_ data type:
	std::vector<std::vector<std::string*>>* lotsOfVectors = new std::vector<std::vector<std::string*>>;

	// Whenever we use the new operator to allocate memory, 
	// we are also responsible to indicating to the OS that we are finished using it
	// We just write delete, then the pointer we got from new.
	delete intValue;
	delete doubleValue;
	delete lotsOfVectors;

	// One big thing with heap allocations is that they continue to exist
	// even after the original value went out of scope
	// The memory remains allocated to our program until we explicitly
	// tell the OS that we're done with it
	return new std::string("This message was allocated elsewhere!");
}

int main() {
	std::string* message = functionWhichAllocatesAString();
	// We can use the pointer just like any other pointer
	std::cout << *message << std::endl;
	// Let's be a good citizen and clean up after ourselves
	delete message;


	// We can also allocate an array using the new[] operator
	// This is basically a list similar to std::vector
	// Except its length cannot be changed
	// And we only have the unsafe (due to no bounds checking) [] operator for indexing it
	// As such, only use this when you absolutely have to
	// Otherwise, it's a much better idea to use std::vector instead
	double* arrayOfDouble = new double[500];

	arrayOfDouble[15] = 200;
	std::cout << arrayOfDouble[15] << std::endl;

	// Another difference is that heap-allocated arrays have no length
	// You need to remember / keep track yourself how long the array you allocated is.
	for(int i = 0; i < 500; i++) {
		std::cout << i << ": " << arrayOfDouble[i] << std::endl;
	}

	// If you used the new[] operator to allocate an array, you MUST use the delete[]
	// operator to delete the memory
	delete[] arrayOfDouble;

	return 0;
}