#include <iostream>

class Satellite {
public:
    std::string name;
    Satellite(std::string _name) : name{_name} {}
};

class Houston {
    Satellite* satellite;
public:
    Houston() {
        satellite = new Satellite("sputnik");
    }
    ~Houston() {
        delete satellite;
        std::cout << "Satellite deorbit completed successfully!" << std::endl;
    }
};

int main() {
    Houston houston;
}