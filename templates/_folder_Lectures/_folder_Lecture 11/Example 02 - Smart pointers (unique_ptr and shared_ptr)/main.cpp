#include <memory>
#include <iostream>

// We will reuse the Printer class from the previous example
// because it allows us to see when its constructor and destructor are called
class Printer {
public:
	void print() {
		std::cout << "Printer was used!" << std::endl;
	}
	Printer() {
		std::cout << "Object created!" << std::endl;
	}
	~Printer() {
		std::cout << "Object destroyed!" << std::endl;
	}
};

// An example below shows how to use this function
void usePrinter(std::unique_ptr<Printer> printer) {
    printer->print();
}

// Same for this one. Note that the parameter is a reference
void alsoUsePrinter(std::unique_ptr<Printer>& printerReference) {
	printerReference->print();
}

// This function shows that it is possible to return a unique_ptr from a function
std::unique_ptr<Printer> createPrinter() {
	std::unique_ptr<Printer> printer {new Printer()};
	// Note: you do not need to use std::move here!
	// Since we're returning a value, we're effectively moving
	// the pointer anyway.
	return printer;
}

int main() {
	// Recommendation: step through this program with a debugger to see
	// where the constructors and destructors are called

	// A unqiue_ptr can be created by either using:
	std::unique_ptr<Printer> printer {new Printer()};

	// Or:
	std::unique_ptr<Printer> printer2 = std::make_unique<Printer>();

	// Note: the () in the line above, this is where parameters to the constructor
	// would go if the class/struct had any.
	// Here's an example using std::string which has a constructor that takes in a value:
	std::unique_ptr<std::string> pointerToString = std::make_unique<std::string>("Hello!");


	// We can use the unique_ptr in exactly the same way as a normal pointer
	// through the Magic of Overloaded Operators
	printer->print();
	(*printer).print();

	for(int i = 0; i < 6; i++) {
		std::unique_ptr<Printer> anotherPrinter {new Printer()};
		Printer* thisWillLeakMemory = new Printer();
		if(i % 2 == 0) {
			continue; // The destructor of anotherPrinter will be called here
			// Since the delete line below will not be executed here,
			// the "thisWillLeakMemory" cause a memory leak
		}
		delete thisWillLeakMemory;
	}

	// You can get hold of the memory address stored within the unique_ptr using the get() method
	printer.get()->print();
	std::cout << "Address stored within printer is: " << printer.get() << std::endl;

	// unique_ptr guarantees that it has the ONLY copy of a pointer
	// It will therefore not allow you to copy it
    // std::unique_ptr<Printer> copy = printer; // error!

	// However, the value stored in the unique_ptr can be transferred to
	// another unique_ptr variable:
    std::cout << "Address stored in printer: " << printer.get() << std::endl; // prints a memory address
    std::unique_ptr<Printer> copyOfPrinter = std::move(printer);
    std::cout << "Address stored in printer: " << printer.get() << std::endl; // prints 0
    std::cout << "Address stored in copyOfPrinter: " << copyOfPrinter.get() << std::endl; // prints the memory address from before

    // The reason we want to transfer a unique_ptr is usually when using it as a parameter
    // of a function
    // In this example, we permanently move our pointer into the function parameter
    // Since the function does not transfer the pointer back when it's done
    // (through a return value), the instance Printer instance stored in printer will 
    // be deallocated and its destructor called when usePrinter() exits.
    usePrinter(std::move(copyOfPrinter));

    // The copyOfPrinter pointer is now empty
    // We can verify that by checking that its pointer is set to nullptr
    if(copyOfPrinter == nullptr) {
    	std::cout << "The copyOfPrinter variable is now nullptr" << std::endl;
    	std::cout << "That means it does not currently contain a memory address of an existing object" << std::endl;
    }

    // There is also a less involved way of passing a unique_ptr into a function
    // And as a bonus, we get to keep our pointer.
    // That is by making the function parameter a reference rather than moving the pointer into it
    std::unique_ptr<Printer> morePrinter {new Printer()};
    // The parameter of alsoUsePrinter() is a reference
    alsoUsePrinter(morePrinter);
    // We therefore keep our pointer, and don't move it. We can still use it:
    morePrinter->print();

    // It is not possible to assign a new value to a unique_ptr once it's empty
    // When you move it into another one, that's it. 
    // While the variable is still there, the value is useless and cannot be changed,
    // so the pointer might as well not exist.

    // It is allowed to return a unique_ptr from a function
    // without having to move it explciitly, because
    // that's more or less what happens anyway

    // However, we have to _initialise_ the receiving pointer (printer3)
    // with the value returned by the function. We can't write
    // printer3 = createPrinter()
    std::unique_ptr<Printer> printer3 {createPrinter()};



    // Now we move on to shared_ptr
    // As opposed to unique_ptr, there can exist multiple copies of a shared_ptr
    std::shared_ptr<Printer> printerShared = std::make_shared<Printer>();
	std::shared_ptr<Printer> printerCopy1 = printerShared;

	// We cna use the use_count() function to see how many copies
	// of the pointer currently exist
    std::cout << "There are now " << printerShared.use_count() << " copies" << std::endl; 
    
    // We can create copies of copies too
    std::shared_ptr<Printer> printerCopy2 = printerCopy1;
    std::shared_ptr<Printer> printerCopy3 = printerShared;
    std::cout << "There are now " << printerShared.use_count() << " copies" << std::endl;

    // When the use_count() of a shared_ptr becomes 0, the memory it references
    // is automatically deleted.

    // A shared_ptr is used just like any other pointer
    // So there is never any need to use std::move

}