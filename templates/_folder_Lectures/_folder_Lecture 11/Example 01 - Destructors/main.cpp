#include <memory>
#include <iostream>
#include <vector>

// We'll first define a class which makes it visible in the terminal
// that its constructor and destructors have been called
// This allows us to see when that happens.
class Printer {
public:
	Printer() {
		std::cout << "Object created!" << std::endl;
	}
	~Printer() {
		std::cout << "Object destroyed!" << std::endl;
	}
};

// We'll use this class for one of the demonstrations
// Its purpose is to show that for a child class, the destructor
// will be called separately for the parent and child
// when an instance of the child class is deleted.
class ChildOfPrinter : public Printer {
public:
	ChildOfPrinter() {
		std::cout << "Child created!" << std::endl;
	}
	~ChildOfPrinter() {
		std::cout << "Child destroyed!" << std::endl;
	}
};

// And another class. This one demonstrates that an object automatically
// calls the destructors of any of its fields. 
// You do not need to do that explicitly.
class PrinterContainer {
    Printer printer;
};

int main() {
	// Change the value of this variable to try any of the demonstrations
	// shown below.
	// Recommendation: step through this program with a debugger
	// this allows you to see where the different constructors/destructors are called
	int demonstration = 0;

	if(demonstration == 0) {
		Printer printer; // constructor called here
	} // destructor called here

	if(demonstration == 1) {
		Printer* printer = new Printer(); // constructor called here
		delete printer; // destructor called here
	}

	if(demonstration == 2) {
		Printer* printer = new Printer[15]; // for each element in this array the constructor is called
		delete printer; // Since we did not use delete[] here, only the destructor for the first element in the array is called.
	}

	if(demonstration == 3) {
		Printer* printer = new Printer[15]; // Constructor is called for each element
		delete[] printer; // Destructor is called for each element
	}

	if(demonstration == 4) {
		// If you look above, you can see that ChildOfPrinter inherits from Printer
		// This means that when an instance of ChildOfPrinter is created,
		// the constructor of both ChildOfPrinter and its parent class Printer are called separately.
		// The same is true for the destructors, which are both called separately.
		// This means that if you allocate some memory using new in the constructor of Printer,
		// you only need to delete that memory in the destructor of Printer.
		// There is no need for ChildOfPrinter to clean up the mess created by Printer, as
		// the constructor of Printer will be called even when a class inherits from it.
		ChildOfPrinter child; // constructor of ChildOfPrinter and Printer are called here
	} // destructor of ChildOfPrinter and Printer are called here

	if(demonstration == 5) {
		// An object containing class or struct members automatically calls
		// any destructors those fields might have.
		// Here PrinterContainer contains a field of type Printer.
		// When an instance of PrinterContainer is created, the constructor of Printer
		// is called automatically, because we are creating an instance of Printer
		// as part of PrinterContainer. 
		// When it's time to delete PrinterContainer, it automatically does the same, and
		// calls the destructor of all fields, including Printer.
		PrinterContainer container; // Constructor of Printer is called here
	} // Destructor of Printer is called here

	if(demonstration == 6) {
		// For the sake of completeness, if you create a std::vector or std::array of an 
		// object, any constructors and destructors are called automatically
		std::vector<Printer> printers(10);
	}
	
	// Conclusion: if you ensure that heap allocated memory is deleted under every
	// circumstance, any destructors are guaranteed to be called!

	return 0;
}