#include <iostream>
#include <memory>

class Domino {
	std::unique_ptr<Domino> nextDomino;
public:
	Domino(std::unique_ptr<Domino> next) : nextDomino(std::move(next)) {

	}
	~Domino() {
		std::cout << "Falling!" << std::endl;
	}
};

int main() {
	Domino domino {
		std::make_unique<Domino>(
			std::make_unique<Domino>(
				std::make_unique<Domino>(
					std::make_unique<Domino>(
						std::make_unique<Domino>(nullptr)
					)
				)
			)
		)
	};
}