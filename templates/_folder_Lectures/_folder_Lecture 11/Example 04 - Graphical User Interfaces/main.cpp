#include <iostream>
#include "AnimationWindow.h"
#include "widgets/Button.h"
#include "widgets/TextInput.h"
#include "widgets/DropdownList.h"

// We'll inherit from AnimationWindow to automatically get a window
class GUIWindow : public TDT4102::AnimationWindow {
	// AnimationWindow has three widget types.
	// Widget 1 of 3 is the Button. 
	// Its callback function is run when the button is clicked
    TDT4102::Button button;

    // Widget 2 of 3 is a text input
    // Its callback function is run when the text within it is changed
    TDT4102::TextInput textInput;

    // Widget 3 of 3 is a dropdown list
    // To create one, we need to supply a vector of strings with the different options
    // a user can select
    std::vector<std::string> dropdownOptions {"Cheese", "Salad", "Tomato"};
    TDT4102::DropdownList dropdownList;

    // All callback functions must have no parameters and must return void
    void buttonClicked() {
        std::cout << "I was clicked!" << std::endl;
    }

    void textInputChanged() {
    	// The text contents of a TextInput can be read using its getText() method
        std::cout << "The contents of the text input is now: " << textInput.getText() << std::endl;
    }

    void dropdownListChanged() {
    	// The value selected by the user in a dropdown list is retrieved using the getValue() method
    	// Note that this method returns one of the strings from the dropdownOptions vector above
        std::cout << "The value of the dropdown list is now: " << dropdownList.getValue() << std::endl;
    }

public:
	// The Button, TextInput, and DropdownList all have their own constructor.
	// They're fairly similar. They all take a position on screen (TDT4102::Point), a width, and a height parameter.
	// The final parameter of Button is the text shown on the button
	// The final parameter of TextInput is the initial text contents (often empty)
	// The final parameter of DropdownList is the vector of strings with the different options that can be selected
    GUIWindow() : button({100, 100}, 100, 30, "Click me!"),
                  textInput({100, 150}, 350, 50, "Change me!"),
                  dropdownList({100, 200}, 200, 50, dropdownOptions) {
      	// Before a widget can be used, it must be added to the window.
        // After we did this, the window will display it and call its callback if the widget has one
        add(button);
        add(textInput);
        add(dropdownList);

        // While usually not every widget has its own callback, 
        // we need to explicitly define which callback function needs to be used when the widget is interacted with
        // Note that multiple widgets can share the same callback function
        // But each widget can only have one single callback function

        // If we want to use a method (like we do in this example),
        // we need to use std::bind() with a pointer to the method to use, with 'this' as the second parameter
        // What std::bind() does, and why we need it is WAY outside the scope of this course
        button.setCallback(std::bind(&GUIWindow::buttonClicked, this));
        textInput.setCallback(std::bind(&GUIWindow::textInputChanged, this));
        dropdownList.setCallback(std::bind(&GUIWindow::dropdownListChanged, this));

        // The TL;DR for why we use std::bind is that we need to know which instance of GUIWindow to call the method for.
    }
};

// Running our application is done by creating an instance of GUIWindow, and calling wait_for_close().
// Any interaction is handled by callback functions
int main() {
    GUIWindow window;

    window.wait_for_close();

	return 0;
}