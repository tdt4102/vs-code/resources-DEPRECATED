#include <iostream>
#include <string>

// A static variable is a variable that stays around in between function calls
// Here we create a static variable called coinState, and initialise it to false
// The next time coinFlip() is called, we invert its value, the corresponding value
// After the function ends, coinState stays around, and will be set to true.
// This means every time this function is called, it is returning another value.
std::string coinFlip() {
	static bool coinState = false;
	coinState = !coinState;
	if(coinState) {
		return "Heads";
	} else {
		return "Tails";
	}
}

// For constants that are used within a class, making the fields static means
// they will not require storage space in every single instance of that class
class Chessboard {
	static constexpr FIELDS_X = 8;
	static constexpr FIELDS_Y = 8;
};

int main() {
	for(int i = 0; i < 10; i++) {
		std::cout << coinFlip() << std::endl;
	}
	return 0;
}


