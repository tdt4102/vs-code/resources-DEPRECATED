#pragma once

class Lamp {
	bool isLit = false;
public:
	void toggleSwitch();
	bool isLightOn();
};