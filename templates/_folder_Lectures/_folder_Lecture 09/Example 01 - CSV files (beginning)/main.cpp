#include <filesystem>
#include <fstream>
#include <vector>
#include <random>

std::vector<double> generateRandomVector(int length) {
	std::vector<double> list(length);

	std::random_device device;
	std::default_random_engine engine(device());
	std::uniform_real_distribution dist(0.0, 100.0);

	for(int i = 0; i < length; i++) {
		list.at(i) = dist(engine);
	}

	return list;
}

int main() {
	const int count = 10;

	std::vector<double> exampleData1 = generateRandomVector(count);
	std::vector<double> exampleData2 = generateRandomVector(count);
	std::vector<double> exampleData3 = generateRandomVector(count);

	return 0;
}