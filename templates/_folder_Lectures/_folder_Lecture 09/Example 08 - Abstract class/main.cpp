// Best practice would be to put these into separate files
// However, I think you have seen enough examples of separate
// files at this point, and this gives you a much better overview

#include <filesystem>
#include <fstream>
#include <vector>
#include <random>
#include <iostream>

class OutputWriter {
public:
    // This function is pure virtual, because it is marked as virtual,
    // and assigned the value 0. This means it is no longer possible
    // to create an instance of OutputWriter, and we must implement it
    // in a class that inherits from this one.
    virtual void writeLine(std::string message) = 0;
};

class FileOutputWriter : public OutputWriter {
    std::ofstream outputStream;
    public:
    FileOutputWriter(std::filesystem::path outputFile) : outputStream{outputFile} {

    }

    void writeLine(std::string message) override {
        outputStream << message << std::endl;
    }
};

class CoutOutputWriter : public OutputWriter {
    void writeLine(std::string message) override {
        std::cout << message << std::endl;
    }
};


void writeResult(double measurement, OutputWriter &writer) {
    writer.writeLine("Measurement: " + std::to_string(measurement));
}

int main() {
    // It is not allowed to create an instance of an abstract class
    // OutputWriter writer; // Error!
    
    FileOutputWriter fileWriter {"measurements.txt"};
    CoutOutputWriter outWriter;
    writeResult(10, fileWriter);
    return 0;
}