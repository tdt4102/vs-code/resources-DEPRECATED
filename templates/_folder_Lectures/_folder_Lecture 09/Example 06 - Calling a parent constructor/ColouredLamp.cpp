#include "ColouredLamp.h"

// When the parent class has a custom constructor,
// you have to call it here explicitly
// It HAS to be  the first thing you initialise
ColouredLamp::ColouredLamp(bool isOn) : Lamp{isOn} {}

bool ColouredLamp::isLightOn() {
	return isLit && lightColour != TDT4102::Color::black;
}