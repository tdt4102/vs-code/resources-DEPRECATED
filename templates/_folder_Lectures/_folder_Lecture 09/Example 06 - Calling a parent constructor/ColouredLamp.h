#pragma once
#include "Lamp.h"
#include "Color.h"

class ColouredLamp : public Lamp {
public:
	ColouredLamp(bool isOn);
	TDT4102::Color lightColour = TDT4102::Color::orange;
	bool isLightOn();
};