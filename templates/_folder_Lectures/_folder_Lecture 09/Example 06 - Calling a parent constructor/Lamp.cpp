#include "Lamp.h"

Lamp::Lamp(bool isOn) : isLit{isOn} {}

void Lamp::toggleSwitch() {
	isLit = !isLit;
}

bool Lamp::isLightOn() {
	return isLit;
}