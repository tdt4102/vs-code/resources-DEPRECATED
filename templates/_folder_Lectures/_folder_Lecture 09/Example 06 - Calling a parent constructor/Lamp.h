#pragma once

class Lamp {
protected:
	bool isLit = false;
public:
	Lamp(bool isOn);
	void toggleSwitch();
	bool isLightOn();
};