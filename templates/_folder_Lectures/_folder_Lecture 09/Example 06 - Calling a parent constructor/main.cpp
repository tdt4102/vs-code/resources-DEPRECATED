#include "ColouredLamp.h"

int main() {
	ColouredLamp lamp(false);

	Lamp basicLamp = static_cast<Lamp>(lamp);

	basicLamp.toggleSwitch();

	return 0;
}