#include <iostream>
#include <string>

struct Point {
	double x = 0;
	double y = 0;

    Point operator+ (Point other) {
        return {x + other.x, y + other.y};
    }
};

int main() {
    Point point1 {5, 6};
    Point point2 {4, 5};
    Point pointSum = point1 + point2; 
	return 0;
}