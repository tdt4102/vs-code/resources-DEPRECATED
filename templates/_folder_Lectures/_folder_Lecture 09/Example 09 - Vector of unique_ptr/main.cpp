// Best practice would be to put these into separate files
// However, I think you have seen enough examples of separate
// files at this point, and this gives you a much better overview

#include <filesystem>
#include <fstream>
#include <vector>
#include <random>
#include <iostream>

class OutputWriter {
public:
    virtual void writeLine(std::string message) = 0;
};

class FileOutputWriter : public OutputWriter {
    std::ofstream outputStream;
    public:
    FileOutputWriter(std::filesystem::path outputFile) : outputStream{outputFile} {

    }

    void writeLine(std::string message) override {
        outputStream << message << std::endl;
    }
};

class CoutOutputWriter : public OutputWriter {
    void writeLine(std::string message) override {
        std::cout << message << std::endl;
    }
};

// To use a unique_ptr in a function, we must pass it by reference.
void writeResult(double measurement, std::unique_ptr<OutputWriter> &writer) {
    // NOTE! To use members (variables or methods) in the writer,
    // we now need to use the -> operator instead of using a .
    writer->writeLine("Measurement: " + std::to_string(measurement));
}

int main() {
    // We cannot put OutputWriter instances into a vector.
    // We instead need something known as a unique_ptr, which we will
    // look at in a later lecture.
    // So for now, just copy and paste this code, and modify it to make
    // it work for the various assignment tasks.
    std::vector<std::unique_ptr<OutputWriter>> writers;

    // We cannot use push_back due to several reasons. 
    // We therefore need to use emplace_back instead.
    writers.emplace_back(new FileOutputWriter {"measurements.txt"});
    writers.emplace_back(new CoutOutputWriter);

    // We can now loop through the contents of the vector,
    // and write a value to each of the OutputWriter instances it contains!
    for(std::unique_ptr<OutputWriter>& writer : writers) {
        writeResult(10, writer);
    }
    return 0;
}