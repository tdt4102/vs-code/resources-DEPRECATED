#include <filesystem>
#include <fstream>
#include <vector>
#include <random>

std::vector<double> generateRandomVector(int length) {
	std::vector<double> list(length);

	std::random_device device;
	std::default_random_engine engine(device());
	std::uniform_real_distribution dist(0.0, 100.0);

	for(int i = 0; i < length; i++) {
		list.at(i) = dist(engine);
	}

	return list;
}

int main() {
	const int count = 10;

	std::vector<double> exampleData1 = generateRandomVector(count);
	std::vector<double> exampleData2 = generateRandomVector(count);
	std::vector<double> exampleData3 = generateRandomVector(count);

	std::filesystem::path outputFilePath{"data.csv"};
	std::ofstream outputStream {outputFilePath};

	outputStream << "Set 1;Set 2;Set 3" << std::endl;
	for(int i = 0; i < count; i++) {
		outputStream << exampleData1.at(i) << ";"
					 << exampleData2.at(i) << ";"
				     << exampleData3.at(i) << std::endl;
	}

	return 0;
}