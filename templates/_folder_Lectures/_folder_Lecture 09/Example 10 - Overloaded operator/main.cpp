#include <iostream>
#include <string>

struct Point {
	double x = 0;
	double y = 0;
};

Point operator+(Point a, Point b) {
    Point sum {a.x + b.x, a.y + b.y};
    return sum;
}

int main() {
    Point point1 {5, 6};
    Point point2 {4, 5};
    Point pointSum = point1 + point2;
	return 0;
}