#include <filesystem>
#include <fstream>
#include <vector>
#include <random>
#include <iostream>

int main() {
	std::filesystem::path filePath{"input.txt"};
	std::ifstream inputStream{filePath};

	// Using the >> operator reads one word,
	// or interprets the word as a value of the data type being read in
	// This behaviour is equivalent to how data is read in from std::cin

	std::string oneWord;
	inputStream >> oneWord;
	std::cout << "Read one word: " << oneWord << std::endl;

	double oneValue;
	inputStream >> oneValue;
	std::cout << "Read one number: " << oneValue << std::endl;

	// getline() reads until the end of the line (in this case we already took a couple of "words"
	// out of that line with what we did above)

	std::string line;
	std::getline(inputStream, line);

	// std::getline() also returns whether you have reached the end of the file
	while(std::getline(inputStream, line)) {
		std::cout << "Line: " << line << std::endl;
	}
}