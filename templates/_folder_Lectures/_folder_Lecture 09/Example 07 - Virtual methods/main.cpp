// Best practice would be to put these into separate files
// However, I think you have seen enough examples of separate
// files at this point, and this gives you a much better overview

#include <filesystem>
#include <fstream>
#include <vector>
#include <random>
#include <iostream>

class OutputWriter {
public:
    virtual void writeLine(std::string message) {
        
    }
};

class FileOutputWriter : public OutputWriter {
    std::ofstream outputStream;
    public:
    FileOutputWriter(std::filesystem::path outputFile) : outputStream{outputFile} {

    }

    void writeLine(std::string message) override {
        outputStream << message << std::endl;
    }
};

class CoutOutputWriter : public OutputWriter {
    void writeLine(std::string message) override {
        std::cout << message << std::endl;
    }
};

// The reference here is crucial!
// Not using it would _convert_ the parameter to an OutputWriter
// Because we use a reference AND the writeLine() method is virtual,
// the method of the class inheriting from OutputWriter will be called!
void writeResult(double measurement, OutputWriter &writer) {
    // We don't care here where OutputWriter writes to, 
    // only that it can write stuff we put in.
    // This makes code highly reusable later, if we decide
    // we want to be able to write stuff to a different place
    // other than cout or an ofstream!
    writer.writeLine("Measurement: " + std::to_string(measurement));
}

int main() {
	FileOutputWriter fileWriter {"measurements.txt"};
    CoutOutputWriter outWriter;
    writeResult(10, fileWriter);
    return 0;
}
