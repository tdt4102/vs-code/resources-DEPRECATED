#include "Lamp.h"

void Lamp::toggleSwitch() {
	isLit = !isLit;
}

bool Lamp::isLightOn() {
	return isLit;
}