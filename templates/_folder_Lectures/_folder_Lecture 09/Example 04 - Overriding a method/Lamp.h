#pragma once

class Lamp {
// The protected keyword allows child classes to use affected fields and methods
// However, functions outside the objects can not use protected members.
protected:
	bool isLit = false;
public:
	void toggleSwitch();
	bool isLightOn();
};