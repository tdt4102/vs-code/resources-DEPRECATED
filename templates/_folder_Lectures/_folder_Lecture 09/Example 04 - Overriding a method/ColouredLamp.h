#pragma once
#include "Lamp.h"
#include "Color.h"

class ColouredLamp : public Lamp {
public:
	TDT4102::Color lightColour = TDT4102::Color::orange;
	// The exact same method is defined in the parent Lamp class
	// And is therefore overridden (overwritten) by this class.
	bool isLightOn();
};