#pragma once
#include "House.h"

// If we want to give access to a single method, 
// we need to declare (but not define the methods of) the class 
// containing the method we want to give access to
class Buddy {
public:
	void openHouseDoor(House &house);
};