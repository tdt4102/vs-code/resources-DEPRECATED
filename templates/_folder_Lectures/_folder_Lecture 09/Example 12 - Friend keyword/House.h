#pragma once

class House {
	bool plantsHaveBeenWatered = false;
	void openFrontDoor();
	
	// All methods in the class Buddy will be able to 
	// call protected and private members (fields and methods)
	friend class Buddy;

	// We can also give a specific method access if we don't want to give
	// access to the entire class. In that case we need to forward declare
	// the containing class if it has at this point not yet been declared
	// (see above how to do that)
	friend void openHouseDoor(House &house);

	// The function useHouseKey will have access to the internals of House
	friend void useHouseKey(House &house);
};