#include "Buddy.h"
#include "House.h"


void useHouseKey(House &house) {
	// We can use private members here, because the function is a friend of House
	// (as declared in House.h)
	house.openFrontDoor();
	house.plantsHaveBeenWatered = true;
}

int main() {
	House house;
	Buddy buddy;
	buddy.openHouseDoor(house);

	useHouseKey(house);

	// main() does not have access to the internals of House
	// Thus, calling openFrontDoor() is not allowed
	// house.openFrontDoor(); // Error!
}