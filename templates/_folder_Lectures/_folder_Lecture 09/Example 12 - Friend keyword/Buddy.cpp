#include "Buddy.h"

void Buddy::openHouseDoor(House& house) {
    // We can use private members here, because Buddy is a friend of House
    house.openFrontDoor();
    house.plantsHaveBeenWatered = true;
}