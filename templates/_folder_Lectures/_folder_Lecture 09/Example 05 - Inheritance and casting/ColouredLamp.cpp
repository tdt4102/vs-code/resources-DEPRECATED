#include "ColouredLamp.h"

bool ColouredLamp::isLightOn() {
	return isLit && lightColour != TDT4102::Color::black;
}