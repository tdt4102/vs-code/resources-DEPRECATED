#include "ColouredLamp.h"

int main() {
	ColouredLamp lamp;

	// You can only cast child classes to their parent.
	Lamp basicLamp = static_cast<Lamp>(lamp);

	// But not the other way around.
	// This would for example not be allowed:
	// ColouredLamp advancedLamp = static_cast<ColouredLamp>(basicLamp);

	// Note: basicLamp is a different instance than lamp
	// Therefore, toggling the switch on basicLamp does not affect the one of lamp
	basicLamp.toggleSwitch();

	return 0;
}