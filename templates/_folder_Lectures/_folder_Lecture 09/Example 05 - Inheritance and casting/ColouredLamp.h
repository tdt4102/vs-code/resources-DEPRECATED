#pragma once
#include "Lamp.h"
#include "Color.h"

class ColouredLamp : public Lamp {
public:
	TDT4102::Color lightColour = TDT4102::Color::orange;
	bool isLightOn();
};