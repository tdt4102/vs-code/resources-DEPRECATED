#pragma once

class Lamp {
protected:
	bool isLit = false;
public:
	void toggleSwitch();
	bool isLightOn();
};