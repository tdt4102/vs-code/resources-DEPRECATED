#define _USE_MATH_DEFINES
#include <AnimationWindow.h>

#include <chrono>
#include <climits>
#include <cmath>
#include <iostream>
#include <random>

#include "widgets/Button.h"
#include "widgets/TextInput.h"

struct Ball {
    float x = 0;
    float y = 0;
    float xSpeed = 0;
    float ySpeed = 0;
    float radius = 10;
    TDT4102::Color colour = TDT4102::Color::white;
};

int x = 10;

inline int randint(int min, int max) {
    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    static std::default_random_engine ran{static_cast<unsigned int>(seed)};
    return std::uniform_int_distribution<>{min, max}(ran);
}

void printMessage() {
    std::cout << "You clicked me!" << std::endl;
}

TDT4102::TextInput input({200, 100}, 300, 40, "Edit me!");

void textChanged() {
    std::cout << "The input text is now: " << input.getText() << std::endl;
}

int main(int argc, char* argv[]) {
    TDT4102::AnimationWindow window(100, 100, 1000, 800, "Test");
    // window.keep_previous_frame(true);

    // window.draw_rectangle({10, 10}, 30, 30, TDT4102::Color::dark_blue);
    // window.wait_for_close();

    TDT4102::Button btn({100, 100}, 100, 40, "Click me!");
    btn.setCallback(&printMessage);
    window.add(btn);

    TDT4102::Button alert_btn({500, 100}, 120, 40, "Don't click!");
    alert_btn.setCallback([&window]() { window.show_info_dialog("DONT CLICK ME!"); });
    window.add(alert_btn);

    input.setCallback(&textChanged);
    window.add(input);

    int x = 10;
    int y = 10;

    TDT4102::Image image("res/unknown.jpg");
    image.width = 100;
    image.height = 100;

    std::vector<Ball> circles;
    circles.resize(5000);
    for (unsigned int i = 0; i < circles.size(); i++) {
        circles.at(i).x = randint(0, window.width());
        circles.at(i).y = randint(0, window.height());
        circles.at(i).radius = randint(5, 10);
        circles.at(i).xSpeed = float(randint(-400, 400)) / 100.0f;
        circles.at(i).ySpeed = float(randint(-400, 400)) / 100.0f;
        circles.at(i).colour = TDT4102::Color(randint(INT_MIN, INT_MAX) & 0xFFFFFF00);
    }
    while (!window.should_close()) {
        if (window.is_key_down(KeyboardKey::A)) {
            x--;
        }
        if (window.is_key_down(KeyboardKey::D)) {
            x++;
        }
        if (window.is_key_down(KeyboardKey::W)) {
            y--;
        }
        if (window.is_key_down(KeyboardKey::S)) {
            y++;
        }
        window.draw_rectangle({x, y}, 15, 15);

        TDT4102::Point mousePosition = window.get_mouse_coordinates();
        window.draw_text({mousePosition.x, mousePosition.y + 100}, std::to_string(mousePosition.x) + ", " + std::to_string(mousePosition.y));
        window.draw_text({mousePosition.x, mousePosition.y + 200}, std::to_string(mousePosition.x) + ", " + std::to_string(mousePosition.y), TDT4102::Color::brown, 70, TDT4102::Font::courier_bold_italic);
        window.draw_arc({500, 500}, mousePosition.x, mousePosition.y, 190, 350, TDT4102::Color::orange);
        window.draw_image(mousePosition, image);
        bool allAtCenter = true;
        for (Ball& ball : circles) {
            float gravityX = (float(window.width()) / 2.0f) - ball.x;
            float gravityY = (float(window.height()) / 2.0f) - ball.y;
            float absoluteSpeed = std::max(std::sqrt(gravityX * gravityX + gravityY * gravityY), 3.0f);
            const float gravity = 0.07f;
            ball.xSpeed += (gravityX / absoluteSpeed) * gravity;
            ball.ySpeed += (gravityY / absoluteSpeed) * gravity;
            ball.xSpeed *= 1.0f - (1.0f / (absoluteSpeed * 2.0f));
            ball.ySpeed *= 1.0f - (1.0f / (absoluteSpeed * 2.0f));
            ball.x += ball.xSpeed;
            ball.y += ball.ySpeed;
            /*if(ball.x - ball.radius > window.w()) {
                ball.x = -ball.radius;
            } else if(ball.x + ball.radius < 0) {
                ball.x = window.w() + ball.radius;
            }
            if(ball.y - ball.radius > window.h()) {
                ball.y = -ball.radius;
            } else if(ball.y + ball.radius < 0) {
                ball.y = window.h() + ball.radius;
            }*/
            if ((std::abs(ball.x - float(window.width()) / 2.0f) > 1.0f) && (std::abs(ball.y - float(window.height()) / 2.0f) > 1.0f)) {
                allAtCenter = false;
            }
            window.draw_circle({int(ball.x), int(ball.y)}, ball.radius, ball.colour, TDT4102::Color::black);
        }
        if (allAtCenter) {
            for (int i = 0; i < circles.size(); i++) {
                double angle = (M_PI / 180.0) * (double(randint(0, 30 * 3600000)) / 10000.0);
                double speed = double(randint(6, 6)) / 50.0;
                circles.at(i).xSpeed = std::cos(angle) * angle * speed;
                circles.at(i).ySpeed = std::sin(angle) * angle * speed;
            }
        }
        window.next_frame();
    }

    return 0;
}