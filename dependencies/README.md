# Animation Window

This is the simple graphics library made for TDT4102.

## Commands

`meson setup builddir` - Set up builddir  
`meson compile -C builddir` - Temporarily cd into builddir and compile  
`meson install -C builddir --destdir ${env:AppData}/tdt4102/animationwindow` - Install files into folder in appdata. Is useful for testing
Note that the `animationwindow.pc` generated is not relocateable. This will not be available as a feature until meson 0.63.0. Therefore we modify line 1 to be `prefix=${pcfiledir}/../..` manually.
