# TDT4102 Resources repo

This repo is now stripped down, so wysiwyg (what you see is what you get).  
REMEMBER: From immediately after the summer of 2022 this repo deprecates the following:

- <https://gitlab.stud.idi.ntnu.no/tdt4102/ovinger/oving-0>
- <https://gitlab.stud.idi.ntnu.no/tdt4102/vs-code/TDT4102>
- <https://gitlab.stud.idi.ntnu.no/tdt4102/vs-code/examples>
- <https://gitlab.stud.idi.ntnu.no/tdt4102/infobank>
- <https://gitlab.stud.idi.ntnu.no/tdt4102/graph_lib>
- <https://gitlab.stud.idi.ntnu.no/tdt4102/ovinger/utdelt>

as they are now files inside the repo instead of subprojects. DELETE these repos to avoid confusion.

## Setup

`git clone https://gitlab.stud.idi.ntnu.no/tdt4102/vs-code/resources` copies the repo to your computer in a subfolder of the folder where you run the command from.

Hot tip for windows users: In File Explorer (filutforsker) navigate to the chosen folder, and then type `cmd` in the navigation bar to open command prompt in the correct folder.

## Walkthrough: Adding articles to infobank

### Do the changes

Change any file in the repo. Make sure you are happy with it.

### Publishing

Just use git. If you don't know git, see mini-tutorial underneath.

`git add .` adds all files to staging area.  
`git commit -m "<message about what you changed>` saves the changes.  
`git push` publishes the changes. The extension will automatically detect that changes are made and pull them.
