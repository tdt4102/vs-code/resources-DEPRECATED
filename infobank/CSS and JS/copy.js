let buttons = document.getElementsByClassName("copy");
for (let button of buttons) {
	button.addEventListener('click', function() {
		let code = button.nextElementSibling.innerText.slice(1); // get the text in the corresponding codeblock and removes \n from beginning
		copyStringToClipboard(code);
	});
}

function copyStringToClipboard (str) {
	let el = document.createElement('textarea');
	el.value = str;
	el.setAttribute('readonly', '');
	el.style = {position: 'absolute', left: '-9999px'};
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
}